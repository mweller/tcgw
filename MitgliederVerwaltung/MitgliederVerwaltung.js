﻿var cors = require("cors");
var express = require("express");
var path = require("path");
var fs = require("fs");
var favicon = require("serve-favicon");
var logger = require("morgan");
var cookieParser = require("cookie-parser");
var bodyParser = require("body-parser");
var compression = require("compression");
var auth = require("./auth");

var routes = require("./routes/index");
var members = require("./routes/members");
var antrag = require("./routes/antrag");
var booking = require("./routes/booking");
var bookingManagement = require("./routes/bookingManagement");
var rechnungGast = require("./routes/rechnungGast");

var newsletter = require("./routes/newsletter");

process.env.TZ = "Europe/Berlin";

var app = express();

var http = require("http");
var https = require("https");

var options = {
  key: fs.readFileSync("cert/key.pem"),
  cert: fs.readFileSync("cert/cert.pem"),
};

var debug = require("debug")("MitgliederVerwaltung");
app.set("port", process.env.PORT || 8089);

//var server = app.listen(app.get('port'), function () {
//    console.log('Mitgliederverwaltung listening on port ' + server.address().port);
//});

// Create an HTTP service.
http.createServer(app).listen(app.get("port"));
console.log("Mitgliederverwaltung listening on port http://" + app.get("port"));

var httpsPort = parseInt(app.get("port")) + 1;
// Create an HTTPS service identical to the HTTP service.
https.createServer(options, app).listen(httpsPort);
console.log("Mitgliederverwaltung listening on port https://" + httpsPort);

app.use(cors());

app.use(compression({ filter: shouldCompress }));

function shouldCompress(req, res) {
  if (req.headers["x-no-compression"]) {
    // don't compress responses with this request header
    return false;
  }

  // fallback to standard filter function
  return compression.filter(req, res);
}

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "jade");

// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger("dev"));
app.use(bodyParser.json({ limit: "20mb" }));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(require("stylus").middleware(path.join(__dirname, "public")));
app.use(express.static(path.join(__dirname, "public")));

app.use("/", routes);
app.use("/booking", booking);

app.use("/newsletter", newsletter);
// app.use("/clubpass", booking);

app.use(auth);
app.use("/members", members.router);
app.use("/antrag", antrag);
app.use("/bookingManagement", bookingManagement);

app.use("/rechnungGast", rechnungGast);

//var jplist = require('./routes/jplist');
//app.use('/jplist', jplist);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  var err = new Error("Not Found");
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get("env") === "development") {
  app.locals.pretty = true;

  app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render("error", {
      message: err.message,
      error: err,
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
  res.status(err.status || 500);
  res.render("error", {
    message: err.message,
    error: {},
  });
});

try {
  process.env.TZ = "Europe/Amsterdam";
} catch (ex) {
  console.error(ex);
}

module.exports = app;
