﻿var auth = require('basic-auth');
var crypto = require('crypto');


var users = {
    'club@tc-kreuzberg.de': {password: '##tc@@', authToken: null},
    'michael@tc-kreuzberg.de': {password: 'mw#0812', authToken: null},
    'holger@tc-kreuzberg.de': {password: 'hk#2019', authToken: null},
    'andrea@tc-kreuzberg.de': {password: 'ad#264', authToken: null},
    'nicole@tc-kreuzberg.de': {password: '2507#nk', authToken: null},

    'manager@tc-kreuzberg.de': {password: 'tiebreak2017', authToken: null},
    'gastronomie': {password: '#gastro#', authToken: null},
};




function parseCookies(request) {
    var list = {},
        rc = request.headers.cookie;

    rc && rc.split(';').forEach(function (cookie) {
        var parts = cookie.split('=');
        list[parts.shift().trim()] = decodeURI(parts.join('='));
    });

    return list;
}



function _login(req, res) {
    var user = req.body.login;
    if (user)
        user = user.toLowerCase().trim();

    if (user && users[user] && (users[user].password == req.body.pass)) {

        var d = new Date();
        var n = d.getTime();

        var s = "xx" + n + "XX";
        s = crypto.createHmac('sha256', s)
            .update('T&C&G&W')
            .digest('hex');

        users[user].authToken = s;
        return s;

    } else {
        return null;
    }

}

function _auth(req, res, next) {

    // for debugging: return next();

    if (req.url == "/favicon.ico")
        return next();

    if (req.url == "")
        return next();


    if (req.url == "/")
        return next();

    if (req.url.startsWith("/booking") && !req.url.toLowerCase().startsWith("/bookingmanagement"))
        return next();


    if (req.url.startsWith("/rechnungGast"))
        return next();

    //
    // no authentication für Mitgliedsanträge
    //
    if (req.url.toLowerCase().indexOf("/antrag") >= 0)
        return next();

    var cookie = parseCookies(req);
    if (cookie["access-token"]) {
        for (var i in users)
            if (cookie["access-token"] == users[i].authToken) {
                if (i == "gastronomie" && (req.url.toLowerCase().indexOf("/members/verzehrumlage") < 0)) {
                    console.log("gastro trys to access: " + req.url);
                    // invalid
                } else {
                    return next();
                }
            }
        res.redirect('/Verwaltung?url=' + req.url);

    } else {

        var user = auth(req);
        if (!user || !users[user.name] || users[user.name].password !== user.pass) {

            res.set('WWW-Authenticate', 'Basic realm="example"');
            res.redirect('/Verwaltung?url=' + req.url);
            return res.status(401).send("Zugriff nicht erlaubt");
        }


        res.redirect('/Verwaltung?url=' + req.url);
        return;

    }
    //    return next();
};


global.login = _login;

module.exports = _auth;
