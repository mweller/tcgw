﻿var express = require("express");
var request = require("sync-request");

var jade = require("jade");
var router = express.Router();

var moment = require("moment");
const { query } = require("express");

global.members = require("./members.js");

router.get("/", function (req, res) {
  res.render("antrag", {});
});

router.get("/antragGespeichert", function (req, res) {
  res.render("antragGespeichert");
});

function sendNotificationMail(member) {
  var fullname = member.FirstName + " " + member.LastName;
  var nodemailer = require("nodemailer");
  var smtpTransport = require("nodemailer-smtp-transport");

  var transporter = nodemailer.createTransport(
    smtpTransport({
      service: "1und1",
      auth: {
        user: "noreply@tc-kreuzberg.de",
        pass: "tiebreak-mail",
      },
      tls: {
        rejectUnauthorized: false,
      },
    })
  );

  var fromMail = "club@tc-kreuzberg.de";
  var toMail = member.Email;

  var subject = "Aufnahmeantrag TCWG eingegangen: " + fullname;

  /////////////////////////////////////////////////////////////////
  //  TestBetrieb für den MailVersand hier ein und ausschalten !!!
  /////////////////////////////////////////////////////////////////
  var testBetrieb = false;
  /////////////////////////////////////////////////////////////////

  var bcc = "";
  bcc += "club@tc-kreuzberg.de" + ";";
  bcc += "finanzen@tc-kreuzberg.de";

  if (testBetrieb) {
    toMail = "mail@michael-weller.de";
    bcc = "";
  }

  var bcc1 = "mail@michael-weller.de";

  var compileTemplate = jade.compileFile("views/antragDokumentation.jade");
  var html = compileTemplate({ member: member });

  transporter.sendMail(
    {
      from: fromMail,
      to: toMail,
      cc: bcc,
      bcc: bcc1,
      subject: subject,
      html: html,
    },
    function (error, info) {}
  );
}

function checkForDuplicate(firstName, lastName) {
  if (firstName == "-neu-") {
    return true;
  }

  if (lastName == "-neu-") {
    return true;
  }

  for (var i in global.members.getMembers()) {
    var member = global.members.getMembers()[i];
    if (firstName == member.FirstName && lastName == member.LastName) {
      return true;
    }
  }

  return false;
}

// Title
// LastName
// FirstName
// Email
// PostCode
// City
// Street
// MemberStatus
// Sex
// BirthDate

// AccountFirstName; AccountLastName; AccountIBAN; AccountBIC; AccountBankName; EnterDate; LeaveDate;

// MasterMemberLastName;
// MasterMemberFirstName;

// Remark

router.post("/stellen", function (req, res) {
  if (checkForDuplicate(req.body.FirstName, req.body.LastName)) {
    res.render("antragDuplicate", { member: member });
  } else {
    var d = moment(new Date()).format("D.M.YYYY");

    // var filename = "Data/AufnahmeAntrag/Offen/" + req.body.FirstName + " " + req.body.LastName + "-" + d + ".json";
    // var content = JSON.stringify(req.body)
    // fs.writeFileSync(filename, content);

    var xmembers = global.members.getMembers();
    var memberId = global.members.createNewMember();
    var member = xmembers[memberId];
    var bd = moment(req.body.BirthDate);
    req.body.BirthDate = bd.format("DD.MM.YYYY");
    for (i in req.body) {
      member[i] = req.body[i];
    }

    if (member.Sex) {
      member.Sex = member.Sex.toLocaleLowerCase();
    }

    member.AufnahmeAntrag = "W"; // AufnahmeAntrag wartet auf Genehmigung
    member.Aktiv = "false";

    var d1 = moment();
    member.EnterDate = d1.format("DD.MM.YYYY");

    if (req.body.MasterMember != "") {
      member.Remark = "Ist Familienmitglied von " + req.body.MasterMember;
    }

    global.members.saveAppendOneMember(member);

    sendNotificationMail(member);

    res.render("antragDokumentation", { member: member });
  }

  // res.redirect(302, '/antrag/antragGespeichert');
});

router.get("/checkiban", function (req, res) {
  var IBAN_NUMBER = req.query.iban;

  var url = "https://openiban.com/validate/" + IBAN_NUMBER + "?getBIC=true&validateBankCode=true";

  var res1 = request("GET", url);

  var data = JSON.parse(res1.getBody("utf8"));
  res.send(JSON.stringify(data));
});

module.exports = router;
