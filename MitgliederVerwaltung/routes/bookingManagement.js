﻿var express = require('express');
var jade = require("jade");

var router = express.Router();
var moment = require('moment');
var fs = require("fs");
var showdown = require('showdown');
var uniqid = require('uniqid');
const members = require('./members.js');


var filenameHotnews = "public/Platzbelegung/hotnews.md";
var filenameFixednews = "public/Platzbelegung/Fixednews.md";

global.members = require("./members.js")

var currentYear = new Date().getFullYear();



function calcPin(memberId) {
    return ((memberId * 1234) & 9765 * currentYear) & 9341;
}

Date.prototype.isLeapYear = function () {
    var year = this.getFullYear();
    if ((year & 3) != 0) return false;
    return ((year % 100) != 0 || (year % 400) == 0);
};

// Get Day of Year
Date.prototype.getDOY = function () {
    var dayCount = [0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334];
    var mn = this.getMonth();
    var dn = this.getDate();
    var dayOfYear = dayCount[mn] + dn;
    if (mn > 1 && this.isLeapYear()) dayOfYear++;
    return dayOfYear;
};

Date.prototype.addDays = function (days) {
    var date = new Date(this.valueOf());
    date.setDate(date.getDate() + days);
    return date;
}


router.get('/', function (req, res) {

    res.render('bookingManagement', null);
});

router.get('/medenspiele', function (req, res) {

    res.render('bookingMedenspiele', null);
});


router.post('/uploadNews', function (req, res) {

    var type = req.query.type

    var news = req.body.data
    if (type == "hotnews") {
        fs.writeFileSync(filenameHotnews, news);
    } else {
        fs.writeFileSync(filenameFixednews, news);
    }

    res.send("@ok")
});

router.get('/manageNews', function (req, res) {

    var params = {}

    var type = req.query.type
    if (type == "hotnews") {
        params = {
            type: type,
            currentContent: fs.readFileSync(filenameHotnews).toString()
        }
    } else {
        params = {
            type: type,
            currentContent: fs.readFileSync(filenameFixednews).toString()
        }

    }

    res.render('manageNews', params);
});

router.post('/ShowPinForMemberIdResult', function (req, res) {
    var memberId = req.body.memberId
    var xmembers = global.members.getMembers()
    var member = xmembers[memberId]

    if (!member) {
        member = {FirstName: "Mitgliedsnummer unbekannt", LastName: ""}
    }

    res.render('ShowPinForMemberIdResult', {member: member, memberId: memberId, pin: calcPin(memberId)})
});

router.get('/ShowPinForMemberId', function (req, res) {

    res.render('ShowPinForMemberId', {})
});


module.exports = router;