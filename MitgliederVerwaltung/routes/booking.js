﻿var express = require("express");
var jade = require("jade");

const fetch = require("node-fetch");

var router = express.Router();
var moment = require("moment");
var fs = require("fs");
var showdown = require("showdown");
var uniqid = require("uniqid");
const members = require("./members.js");

var filenameHotnews = "public/Platzbelegung/hotnews.md";
var filenameFixednews = "public/Platzbelegung/fixednews.md";

global.members = require("./members.js");

var currentYear = new Date().getFullYear();

function calcPin(memberId) {
  return (memberId * 1234) & (9765 * currentYear) & 9341;
}

Date.prototype.isLeapYear = function () {
  var year = this.getFullYear();
  if ((year & 3) != 0) return false;
  return year % 100 != 0 || year % 400 == 0;
};

// Get Day of Year
Date.prototype.getDOY = function () {
  var dayCount = [0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334];
  var mn = this.getMonth();
  var dn = this.getDate();
  var dayOfYear = dayCount[mn] + dn;
  if (mn > 1 && this.isLeapYear()) dayOfYear++;
  return dayOfYear;
};

Date.prototype.addDays = function (days) {
  var date = new Date(this.valueOf());
  date.setDate(date.getDate() + days);
  return date;
};

Date.prototype.addHours = function (hours) {
  var date = new Date(this.valueOf());
  date.setHours(date.getHours() + hours);
  return date;
};

function ensureInt(i) {
  if (typeof i == "number") {
    return i;
  } else return parseInt(i);
}

router.post("/doregister", function (req, res) {
  var data = req.body;

  var memberId = data.memberId;

  var pinSoll = calcPin(memberId);

  var pinIst = data.pin;

  if (pinIst != pinSoll) {
    res.send(
      JSON.stringify({
        error: "Mitgliedsnummer oder PIN ist falsch",
      })
    );
    return;
  }

  var xmembers = global.members.getMembers();

  sRes = JSON.stringify({ error: "falsche Mitgliedsnummer" });

  if (xmembers != null) {
    var member = xmembers[memberId];
    if (member != null)
      if (member.Aktiv != "false") {
        sRes = JSON.stringify({
          memberId: member.MemberID,
          firstName: member.FirstName,
          lastName: member.LastName,
        });
      }
  }

  res.send(sRes);
});

router.get("/register", function (req, res) {
  var pin = req.query.pin;
  var id = req.query.id;
  var error = req.query.error;

  if (!pin || pin == "null") {
    pin = "";
  }

  if (!id || id == "null") {
    id = "";
  }

  var params = {};
  params.pin = pin;
  params.id = id;

  if (error) {
    params.error = error;
  }

  res.render("bookingRegister", params);
});

router.get("/autoregister", function (req, res) {
  var pin = req.query.pin;
  var id = req.query.id;

  var xmembers = global.members.getMembers();

  sRes = JSON.stringify({ error: "falsche Mitgliedsnummer" });

  if (xmembers != null) {
    var member = xmembers[id];
    var name = member.FirstName + " " + member.LastName;
    res.render("bookingRegister", { name: name, pin: pin, id: id });
  } else {
    res.render("bookingRegister", {});
  }
});

function formatIBAN(iban) {
  var ibanF = "";
  for (var i = 0; i <= 5; i++) {
    ibanF = ibanF + iban.substr(0, 4) + " ";
    iban = iban.substr(4, iban.length);
  }
  return ibanF;
}

router.get("/getIBAN", function (req, res) {
  var id = req.query.id;

  var xmembers = global.members.getMembers();
  if (xmembers != null) {
    var member = xmembers[id];
    if (member && member.Aktiv != "false") {
      if (!member.AccountIBAN) {
        member = xmembers[member.MasterMemberID];
      }

      if (member && member.AccountIBAN) {
        var resData = {
          iban: member.AccountIBAN,
          ibanF: formatIBAN(member.AccountIBAN),
          bic: member.AccountBIC,
          bankName: member.AccountBankName,
          kontoInhaber: member.AccountFirstName + " " + member.AccountLastName,
        };

        if (member.AccountLastName) {
          resData.kontoInhaber = member.AccountFirstName + " " + member.AccountLastName;
        } else {
          resData.kontoInhaber = member.FirstName + " " + member.LastName;
        }

        res.send(resData);
        return;
      }
    }
  }

  res.send("@error");
});

router.get("/checkvalid", function (req, res) {
  var pin = req.query.pin;
  var id = req.query.id;

  if (id == 97531) {
    res.send("@ok");
    return;
  }

  var xmembers = global.members.getMembers();
  if (xmembers != null) {
    var member = xmembers[id];
    if (member && member.Aktiv != "false") {
      if (calcPin(id) == pin) {
        res.send("@ok");
        return;
      }
    }
  }

  res.send("@error");
});

router.get("/reset", function (req, res) {
  var s = `<Script>
        localStorage.removeItem('user');
        window.location.href = "/booking/register";
    </script>`;

  res.send(s);
});

router.get("/faq", function (req, res) {
  converter = new showdown.Converter();
  var filename = "public/Platzbelegung/faq.md";

  var htmlFAQ = "";

  if (fs.existsSync(filename)) {
    var data = fs.readFileSync(filename).toString();
    htmlFAQ = converter.makeHtml(data);
    // req.query.htmlFAQ = htmlFAQ
  }

  res.render("bookingfaq", { htmlFAQ: htmlFAQ });
});

router.get("/", function (req, res) {
  converter = new showdown.Converter();

  if (fs.existsSync(filenameHotnews)) {
    var data = fs.readFileSync(filenameHotnews).toString();
    htmlHotNews = converter.makeHtml(data);
    req.query.htmlHotNews = htmlHotNews;
  }

  if (fs.existsSync(filenameFixednews)) {
    var data = fs.readFileSync(filenameFixednews).toString();
    htmlFixedNews = converter.makeHtml(data);
    req.query.htmlFixedNews = htmlFixedNews;
  }

  res.render("booking", req.query);
});

function fixMissingUids(bookings) {
  var fixed = false;
  for (var b in bookings) {
    var booking = bookings[b];
    if (booking.uid) {
    } else {
      booking.uid = uniqid();
      fixed = true;
    }
  }
  return fixed;
}

function loadBookings(day) {
  const year = new Date().getFullYear();
  var filename = `public/Platzbelegung/${year}/${day}.json`;

  var bookings = [];
  if (fs.existsSync(filename)) {
    var content = fs.readFileSync(filename);
    bookings = JSON.parse(content);
  }

  //
  // aus datenschutzgründen die Kontodaten für gästebuchungen löschen
  //
  for (b in bookings) {
    var booking = bookings[b];
    if (booking.gastData) {
      booking.gastData = {};
    }
  }

  if (fixMissingUids(bookings)) {
    saveBookings(day, bookings);
  }

  return bookings;
}

function saveBookings(day, data) {
  const year = new Date().getFullYear();

  const dir = `public/Platzbelegung/${year}`;
  if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir);
  }

  var filename = `${dir}/${day}.json`;

  var content = JSON.stringify(data);
  fs.writeFileSync(filename, content);
}

function loadDeletedBookings(day) {
  const year = new Date().getFullYear();
  var filename = `public/Platzbelegung/${year}/${day}-deleted.json`;

  var bookings = [];
  if (fs.existsSync(filename)) {
    var content = fs.readFileSync(filename);
    bookings = JSON.parse(content);
  }

  return bookings;
}

function saveDeletedBookings(day, data) {
  const year = new Date().getFullYear();

  const dir = `public/Platzbelegung/${year}`;
  if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir);
  }

  var filename = `${dir}/${day}-deleted.json`;

  var content = JSON.stringify(data);
  fs.writeFileSync(filename, content);
}

function check_BereitsGebucht(data) {
  const msToHours = 1.0 / 1000 / 60 / 60;

  var d = new Date();
  var day = d.getDOY();

  if (data.partners != null && data.partners.length > 0) {
    //
    // SpezialIds für Medenspiele/Training/Veranstaltungen etc.
    //
    var partner0 = data.partners[0];
    var id = partner0.split(";")[0];
    if (parseInt(id) < 20) {
      return null;
    }
  }

  var isSpontan = false;
  var result = "";

  //
  // prüfe auf "SpontanBuchung"
  // ... bis 14 Uhr erlaubt.
  //

  const dateCreated = new Date(data.dateCreated);
  const dateNewBooking = new Date(data.date);

  var hoursAbsoluteNow = dateCreated.getTime() * msToHours;
  var hoursAbsoultNewBooking = dateNewBooking.getTime() * msToHours;

  var hoursOfDayNewBooking = dateNewBooking.getHours() + dateNewBooking.getMinutes() / 60;

  var spontalFailure = null;

  //
  // bis 14:00 ist eine Spontanbuchung 2 Stunden im voraus erlaubt
  //
  if (hoursOfDayNewBooking <= 14.55) {
    if (hoursAbsoultNewBooking < hoursAbsoluteNow + 2) {
      isSpontan = true;
    } else {
      spontalFailure = "Spontan kann man bis 14 Uhr nur 2 Stunden im Vorraus buchen";
    }

    //
    // danach ist eine Spontalbuchung offizell nur noch 15 Minuten im Voraus erlaubt
    // tatsächlich prüfen wir auf gut 30 Minuten
    //
    if (hoursAbsoultNewBooking < hoursAbsoluteNow + 0.51) {
      isSpontan = true; // gute halbe stunde früher
    } else {
      spontalFailure = "Spantan kann man nach 14 Uhr nur 15 Minuten im Vorraus buchen";
    }
  } else {
    // spontalFailure = "Spontal kann man nur für heutre buchen"
  }

  //
  // Ende der Spontan Buchungen
  //

  //
  // Prüfe Regel für Gastbuchung
  //

  for (var p in data.partners) {
    var partner = data.partners[p];

    if (partner.indexOf("9999") >= 0) {
      // 9999 == Gast

      var numHours = 1;
      if (data.numHours) {
        numHours = parseInt(data.numHours);
      }

      const xbookEnd = hoursAbsoultNewBooking + numHours;

      const deltaHours = hoursAbsoultNewBooking - hoursAbsoluteNow;

      if (xbookEnd < 15.55) {
        // ok
      } else if (deltaHours < 24) {
        // ok
      } else {
        return "Gastbuchungen sind nur 24 Stunden im Voraus,\n oder mit Ende bis 15:30 möglich";
      }
    }
  }

  //
  // Ende Gastbuchung
  //

  var result = "";

  for (var i = 0; i < 7; i++) {
    // für die nächsten 7 Tage

    console.log("check day " + i);
    var sDay = "in " + i + " Tagen";
    switch (i) {
      case 0:
        sDay = "Heute";
        break;
      case 1:
        sDay = "Morgen";
        break;
      case 2:
        sDay = "Übermorgen";
        break;
    }

    if (data.partners && data.partners.length == 2) {
      if (data.partners[0] == data.partners[1]) {
        data.partners = [data.partners[0]];
      }
    }

    var bookings = loadBookings(day + i);
    for (b in bookings) {
      // für alle Buchungen des Tages

      var booking = bookings[b];

      if (booking.uid != data.uid) {
        //
        // same booking. zb für move, ab 2023 löscht der Client allerdings die alte Buchung erst
        //
        for (var p in booking.partners) {
          // für alle Partner der bestehenden Buchung
          var partner = booking.partners[p];

          for (var p1 in data.partners) {
            // für alle Partner der neuen Buchung

            var partner1 = data.partners[p1];
            console.log(day + i + ":" + partner + " " + partner1);

            if (partner == partner1 && partner.indexOf("9999") < 0) {
              // 9999 == Gast
              //
              // Kein Gast ....
              // Gäste kann es mehrfach geben
              //

              var numHours = 1;
              if (booking.numHours) {
                numHours = parseInt(booking.numHours);
              }

              const bookingDate = new Date(booking.date);
              const sBookingTime = bookingDate.toLocaleTimeString("de-DE", { hour: "2-digit", minute: "2-digit" });

              var bookingTimeAbsolut = bookingDate.getTime() * msToHours; // in stunden absolut
              var bookingEndTimeAbolut = bookingTimeAbsolut + numHours;
              var bookingEndTime1 = bookingEndTimeAbolut + 0.25;

              if (hoursAbsoluteNow > bookingEndTime1) {
                //
                // eine viertel Stunde nach dem Ende der Buchung, kann man neu buchen
                //
                // ok
              } else {
                //
                // Ansonsten liegt eine Doppelbuchung vor
                //

                const sParterName = partner.split(";")[1];
                const sParterFirstname = sParterName.split(" ")[0];

                result = (result || "") + `${sParterFirstname} ist schon verabredet: ${sDay} ${sBookingTime}  Platz ${booking.court}\n`;
              }
            }
          }
        } // for partners
      } // ui !=
    } // for bookings of one day
  } // for days

  if (isSpontan) {
    return "@ok spontan";
  } else {
    if (spontalFailure && result) {
      result = spontalFailure + "\n\n" + result;
    }
    return result || "@ok";
  }
}

function check_Belegt(data) {
  const dataDate = new Date(data.date);

  var day = dataDate.getDOY();
  var bookings = loadBookings(day);

  var dataNumHours = 1;
  if (data.numHours) {
    dataNumHours = parseInt(data.numHours);
  }
  var dataTstart = dataDate.getHours() + dataDate.getMinutes() / 60;
  var dataTend = dataTstart + dataNumHours;

  for (b in bookings) {
    var booking = bookings[b];

    const dateBooking = new Date(booking.date);

    var bookingNumHours = 1;
    if (booking.numHours) {
      bookingNumHours = ensureInt(booking.numHours);
    }

    var bookingTstart = dateBooking.getHours() + dateBooking.getMinutes() / 60;
    var bookingTend = bookingTstart + bookingNumHours;

    var c1 = booking.court;
    var c2 = c1;
    if (booking.numCourts) {
      c2 = ensureInt(booking.court) + ensureInt(booking.numCourts) - 1;
    }

    if (c1 <= ensureInt(data.court) && ensureInt(data.court) <= c2) {
      var belegt = bookingTstart < dataTend && bookingTend > dataTstart;

      if (belegt) {
        return `Platz ${booking.court} ist belegt`;
      }
    }
  }

  return null;
}

function isGuestBooking(booking) {
  for (var p in booking.partners) {
    var partner = booking.partners[p];
    if (partner.indexOf("GAST") >= 0) {
      return true;
    }
  }
  return false;
}

function check_all(data) {
  var err = check_Belegt(data);
  if (err) return err;

  var err = check_BereitsGebucht(data);
  return err;
}

function convertPartners(data) {
  if (data.partners == undefined) {
    if (typeof data["partners[]"] == "string") {
      data.partners = [data["partners[]"], data["partners[]"]];
    } else {
      data.partners = data["partners[]"];
    }
    delete data["partners[]"];
  }
}

router.post("/checkBookingAllowed", function (req, res) {
  var data = req.body;

  convertPartners(data);

  var err = check_all(data);

  if (err) {
    res.send(err);
    return;
  }

  res.send("@ok");
});

function findBookingIndexByUid(bookings, uid) {
  for (var i in bookings) {
    if (bookings[i].uid == uid) {
      return i;
    }
  }

  return null;
}

function addPublicBookingFlags(bookings) {
  var xmembers = global.members.getMembers();

  for (var i in bookings) {
    var booking = bookings[i];
    for (var p in booking.partners) {
      var partner = booking.partners[p];
      if (partner) {
        var id = partner.split(";")[0];
        var member = xmembers[id];
        if (member) {
          if (member.PublicBooking != "false") {
            partner += ";#pb";
            booking.partners[p] = partner;
          }
        }
      }
    }
  }
}

router.post("/moveBooking", function (req, res) {
  var data = req.body;
  convertPartners(data);

  var err = check_all(data);
  if (err && !(err.substr(0, 3) == "@ok")) {
    res.send(err);
    return;
  }

  if (data.uid) {
  } else {
    res.send("@system error: no uid");
    return;
  }

  var d = new Date(data.date);
  var day = d.getDOY();

  var bookings = loadBookings(day);
  var bookingIndex = findBookingIndexByUid(bookings, data.uid);
  if (bookingIndex) {
    bookings[bookingIndex] = data;
    saveBookings(day, bookings);
  } else {
    res.send("@system error: uid not found: " + data.uid);
    return;
  }

  // res.send("Doppeltbuchung")

  res.send("@ok");
});

function findInvalidPartner(data) {
  if (data.partners) {
    if (data.partners.length != 2 && data.partners.length != 4) {
      return "Falsche Anzahl von Partnen: " + data.partners.length;
    }

    var xmembers = global.members.getMembers();

    for (var i = 0; i < data.partners.length; i++) {
      var partner = data.partners[i];
      var id = partner.split(";")[0];
      if ((id > 0 && id < 20) || id == 9999 || xmembers[id]) {
      } else {
        return "Problem mit Partner: <" + partner + ">";
      }
    }
  }

  return null;
}

router.post("/checkPlaceholderBooking", function (req, res) {
  var data = req.body;
  convertPartners(data);

  var errPartner = findInvalidPartner(data);
  if (errPartner) {
    res.send(errPartner);
    return;
  }

  const isSpezialBuchung = data.partners && data.partners.length > 0 && parseInt(data.partners[0].split(";")[0]) <= 10;
  if (!isSpezialBuchung) {
    res.send("@ok");
    return;
  }

  //
  // 1. prüfen ob eine gelöschte Buchung für die gleiche Uhrzeit auf dem gleichen Platz am gleichen Tag vorliegt
  //    Wenn Nein: Alles gut
  //

  // kommen in der gelöschten Buchung vor, aber nicht in der aktuellen
  var platzhalter = [];

  // kommen in der aktuellen Buchung vor, aber nicht in der gelöschten
  var ersetzer = [];

  //
  // 2. prüfen ob es eine Übereinstimmung der Personen bei der gelöschten und der aktuellen Buchung gibt
  //    Wenn Nein: Alles gut

  //
  // 3. für alle Personen die "neu in der aktuellen Buchung sind"
  //    Prüfen ob diese nach dem Zeitpunkt der Oiginalbuchung (der gelöschten Buchung) noch eibe Buchung hatten.
  //    Wenn Nein: Person aus der Liste streichen
  //

  //
  // Wenn wir hier an kommen und die Liste der Personen nicht leer ist,
  // haben wir eine Platzhalterbuchung
  //

  //
  // In diesem Falle geben wir eine Liste der Platzhalter und eine Liste der für diese eingesetzten Personen zurück
  //

  res.send({ platzhalter: platzhalter, ersetzer: ersetzer });
});

router.post("/book", function (req, res) {
  var data = req.body;
  convertPartners(data);

  var errPartner = findInvalidPartner(data);
  if (errPartner) {
    res.send(errPartner);
    return;
  }

  const isSpezialBuchung = data.partners && data.partners.length > 0 && parseInt(data.partners[0].split(";")[0]) <= 10;
  if (!isSpezialBuchung) {
    if (data.partners && data.partners.length > 2) {
      data.numHours = 2;
    }
  }

  var err = check_all(data);
  if (err && !(err.substr(0, 3) == "@ok")) {
    res.send(err);
    return;
  }

  if (data.uid) {
  } else {
    data.uid = uniqid(); // -> '6c84fb90-12c4-11e1-840d-7b25c5ee775a'
  }

  var d = new Date(data.date);
  var day = d.getDOY();

  if (isNaN(day)) {
    res.send("Systemfehler: Datum");
    return;
  }

  var bookings = loadBookings(day);
  bookings.push(data);
  saveBookings(day, bookings);

  if (data.uidSerie) {
    for (var d1 = day + 7; d1 < 330; d1 = d1 + 7) {
      d = d.addDays(7);
      data.date = d;
      var bookings = loadBookings(d1);
      bookings.push(data);
      saveBookings(d1, bookings);
    }
  }

  // res.send("Doppeltbuchung")

  res.send("@ok");
});

router.post("/bookraw", function (req, res) {
  var data = req.body;

  data.uid = uniqid(); // -> '6c84fb90-12c4-11e1-840d-7b25c5ee775a'

  var d = new Date(data.date);
  var day = d.getDOY();

  if (isNaN(day)) {
    res.send("Systemfehler: Datum");
    return;
  }

  var bookings = loadBookings(day);
  bookings.push(data);
  saveBookings(day, bookings);

  res.send("@ok");
});

function deleteAllMedenspielBookings() {
  for (var day = 100; day < 300; day++) {
    var bookings = loadBookings(day);
    if (bookings.length > 0) {
      var deleted = false;
      for (var b = 0; b < bookings.length; b++) {
        var booking = bookings[b];
        if (booking.type == "Medenspiel") {
          deleted = true;
          bookings.splice(b, 1);
          b--;
        }
      }

      if (deleted) {
        saveBookings(day, bookings);
      }
    }
  }
}

function insertMedenspielBookings(data) {
  try {
    lines = data.split("\n");
    for (var i = 1; i < lines.length; i++) {
      var line = lines[i];
      var rowRaw = line.split(";");
      if (rowRaw.length >= 5) {
        try {
          row = {
            raw: rowRaw,
            type: "Medenspiel",
            sDate: rowRaw[0].trim(),
            hour: rowRaw[1].trim(),
            endTime: rowRaw[2],
            courts: rowRaw[3],
            match: rowRaw[4],
          };

          var ss = row.hour.split(":");
          row.hour = parseInt(ss[0]);
          row.minutes = ss[1];

          ss = row.endTime.split(":");
          var endHourX = parseInt(ss[0]);
          row.endHour = endHourX + parseInt(ss[1]) / 60;

          row.numHours = row.endHour - row.hour;

          row.text = row.match;

          ss = row.courts.split(",");
          row.court = parseInt(ss[0]);

          row.numCourts = ss.length;

          try {
            var ss = row.sDate.split(".");

            var d = new Date(ss[2], parseInt(ss[1]) - 1, ss[0]);

            var d1 = new Date(ss[2], parseInt(ss[1]) - 1, ss[0], row.hour, row.minutes);
            row.date = d1;

            var d2 = new Date(ss[2], parseInt(ss[1]) - 1, ss[0]);
            row.dayIndex = d2.getDOY();
          } catch (ex) {
            console.error(ex);
          }

          row.uid = uniqid();

          var day = d.getDOY();
          var bookings = loadBookings(day);
          bookings.push(row);
          saveBookings(day, bookings);
        } catch (ex) {
          console.error(ex);
        }
      }
    }
  } catch (ex) {
    console.error(ex);
  }
}

router.post("/bookmedenspiele", function (req, res) {
  var data = req.body.medenspiele;
  deleteAllMedenspielBookings();
  insertMedenspielBookings(data);
  res.send("@ok");
});

router.post("/delete", function (req, res) {
  var data = req.body;

  var d = new Date(data.date);
  var day = d.getDOY();

  var bookings = loadBookings(day);
  for (var i = 0; i < bookings.length; i++) {
    var booking = bookings[i];
    if (booking && booking.uid == data.uid) {
      bookings.splice(i, 1);
      saveBookings(day, bookings);

      // backup deleted bookings

      let deletedBookings = loadDeletedBookings(day);
      deletedBookings.push(booking);
      saveDeletedBookings(day, deletedBookings);

      res.send("@ok");
      return;
    }
  }

  res.send("@notfound");
});

router.post("/deleteSerie", function (req, res) {
  var data = req.body;

  var d = new Date(data.date);
  var day = d.getDOY();

  var found = 0;

  for (var d = day; d < 330; d = d + 7) {
    var bookings = loadBookings(d);
    for (var i = 0; i < bookings.length; i++) {
      var booking = bookings[i];
      if (booking && booking.uidSerie && booking.uidSerie == data.uidSerie) {
        bookings.splice(i, 1);
        found = found + 1;
        saveBookings(d, bookings);
      }
    }
  }

  if (found > 0) {
    res.send("@ok\n" + found);
  } else {
    res.send("@notfound");
  }
});

router.get("/checkKreuzbergMode", function (req, res) {
  var ip = req.header("x-forwarded-for") || req.connection.remoteAddress;
  if (global.ipClubhaus == ip) {
    return res.send("@ok");
  } else {
    return res.send("@error\n clubHausIp=" + global.ipClubhaus + "\n yourIp=" + ip);
  }
});

router.post("/getbookings", function (req, res) {
  var startData = req.body.date;

  console.log("get bookings");

  if (req.body.anzeigeTafelMode == "true") {
    //
    // die Anzeigetafel wir über das WLAN des Clubhauses betrieben,
    // somit können wir uns die IP Adresse merken als diejenige des Clubhaus WLANs
    //
    var ip = req.header("x-forwarded-for") || req.connection.remoteAddress;
    global.ipClubhaus = ip;
  }

  const numdays = req.body.numdays || 3;

  var d = new Date(startData);
  var day = d.getDOY();

  var resData = [];
  for (var i = 0; i < numdays; i++) {
    var bookings = loadBookings(day + i);
    addPublicBookingFlags(bookings);
    resData[day + i] = bookings;
  }

  res.send(JSON.stringify(resData));
});

router.get("/memberlist", function (req, res) {
  var xmembers = global.members.getMembers();

  sRes = "";
  if (xmembers != null) {
    for (var i in xmembers) {
      var member = xmembers[i];
      if (member.Aktiv != "false") {
        sRes += `${member.MemberID};${member.FirstName} ${member.LastName}\n`;
      }
    }
  }

  res.send(sRes);
});

router.get("/getpublicbooking", function (req, res) {
  var xmembers = global.members.getMembers();

  var id = req.query.id;

  var sRes = "private";
  if (xmembers != null) {
    var member = xmembers[id];
    if (member.PublicBooking == "true") {
      sRes = "public";
    }
  }

  res.send(sRes);
});

router.get("/setpublicbooking", function (req, res) {
  var xmembers = global.members.getMembers();

  var id = req.query.id;
  var value = req.query.value;

  var sRes = "@error";
  if (xmembers != null) {
    var member = xmembers[id];
    if (member) {
      if (member.PublicBooking != value) {
        member.PublicBooking = value;
        global.members.saveAppendOneMember(member);
      }

      sRes = "@ok";
    }
  }

  res.send(sRes);
});

function findMemberWithEmail(email) {
  var xmembers = global.members.getMembers();

  for (var m in xmembers) {
    var member = xmembers[m];
    var e1 = member.Email;
    var e2 = email.trim().toLowerCase();
    if (e1 && e2) {
      e1 = e1.trim().toLowerCase();
      if (e1 == e2) {
        return member;
      }
    }
  }
}

function sendPinEmail(member) {
  members.compileRechnung("PinBrief");
  members.CreateAndSaveRechnung("PinBrief", member.MemberID);

  var ResultList = [];
  members.SendOneMail(member.MemberID, "PinBrief", ResultList);
}

router.get("/emailForPinNotSend", function (req, res) {
  res.render("emailForPinNotSend", { email: "xxx" });
});
router.get("/emailForPinSend", function (req, res) {
  res.render("emailForPinSend", { email: "xxx" });
});

router.get("/pinanfordern", function (req, res) {
  res.render("pinanfordern", { email: "xxx" });
});

router.post("/pinanfordern", function (req, res) {
  var email = req.body.email;
  var memberId = req.body.memberId;

  var member = null;

  if (email != null) {
    member = findMemberWithEmail(email);
  } else {
    var xmembers = global.members.getMembers();
    member = xmembers[memberId];
  }

  if (member != null && member.Aktiv != "false") {
    sendPinEmail(member);
    res.redirect(302, "/booking/emailForPinSend");
    return;
  }

  res.redirect(302, "/booking/emailForPinNotSend");
});

function isBookingInThePast(booking) {
  var d = new Date(booking.date);
  d = d.addHours(1);
  return d < new Date();
}

function isBookingWithId(memberId, booking) {
  for (var p in booking.partners) {
    var partner = booking.partners[p];
    const ss = partner.split(";");
    if (ss[0] == memberId) {
      return true;
    }
  }

  return false;
}

router.get("/getMyNextBooking", function (req, res) {
  var id = req.query.id;

  var myNextBookingInfo = undefined;

  var day = new Date().getDOY();

  var bookings = loadBookings(day);

  const numdays = 3;
  for (var i = 0; i < numdays; i++) {
    var bookings = loadBookings(day + i);
    for (var b in bookings) {
      var booking = bookings[b];

      if (isBookingWithId(id, booking) && !isBookingInThePast(booking)) {
        myNextBookingInfo = booking;
        break;
      }
    }

    if (myNextBookingInfo) {
      break;
    }
  }

  res.send(JSON.stringify(myNextBookingInfo));
});

router.get("/getServerTime", function (req, res) {
  res.send(new Date().toLocaleDateString() + "<br/>" + new Date().toLocaleTimeString());
});

router.get("/hotnews", function (req, res) {
  converter = new showdown.Converter();

  if (fs.existsSync(filenameHotnews)) {
    var data = fs.readFileSync(filenameHotnews).toString();
    htmlHotNews = converter.makeHtml(data);
    res.send(htmlHotNews);
  } else {
    res.send("@error");
  }
});

router.get("/fixednews", function (req, res) {
  converter = new showdown.Converter();

  if (fs.existsSync(filenameFixednews)) {
    var data = fs.readFileSync(filenameFixednews).toString();
    fixedNews = converter.makeHtml(data);
    res.send(fixedNews);
  } else {
    res.send("@error");
  }
});

router.get("/sendPinbrief", function (req, res) {
  try {
    var xmembers = global.members.getMembers();
    var member;

    const email = req.query.email;
    var memberId = undefined;
    for (var i in xmembers) {
      if (xmembers[i].Email && xmembers[i].Email.toLowerCase().trim() == email.toLowerCase().trim()) {
        memberId = xmembers[i].MemberID;
        member = xmembers[i];
        break;
      }
    }

    if (memberId && member) {
      var nodemailer = require("nodemailer");

      var transporter = nodemailer.createTransport({
        host: "mail.incom.de",
        port: 25, // 465,
        secure: false,
        auth: {
          user: "tcgw",
          pass: "Admin123!",
        },
        tls: {
          rejectUnauthorized: false,
        },
      });

      var fromMail = "Verwaltung TC GW Am Kreuzberg e.V.<club@tc-kreuzberg.de>";
      var subject = "Zugang zur TCGW Buchungsapp";
      var bcc = "";

      ///////////s//////////////////////////////////////////////////////
      //  TestBetrieb für den MailVersand hier ein und ausschalten !!!
      /////////////////////////////////////////////////////////////////
      var testBetrieb = false;
      /////////////////////////////////////////////////////////////////

      if (testBetrieb) {
        toMail = "mail@michael-weller.de";
        subject = "TEST: " + member.Email + " " + subject;
        fromMail = "Verwaltung TC GW Am Kreuzberg e.V.<mail@michael-weller.de>";
      }

      html = `
      <div> 
         Herzlich willkommen ${member.FirstName} bei der TCGW Platzbuchung" <br/>
         <br/>
         Deine Mitgliednummer ist: ${memberId} <br/>
         Deine Pin ist: ${calcPin(memberId)} <br/>
         <br/>
         <a href="https://app.tc-kreuzberg.de/autoregister?id=${memberId}&pin=${calcPin(memberId)}"> 
            Klicke hier für einen direkten Zugang zur Buchungsapp 
         </a> <br>

      </div>
      `;

      transporter.sendMail(
        {
          from: fromMail,
          to: member.Email,
          bcc: bcc,
          subject: subject,
          html: html,
        },
        function (error, info) {
          try {
            if (error) {
              res.send("@error: email not found: " + email);
            } else {
              res.send("@ok");
            }
          } catch (ex) {
            console.log("Error in _sendOneMail, transporter.sendMail, callback: " + ex.message);
          }
        }
      );
    } else {
      res.send("@error: email not found: " + email);
    }
  } catch (ex) {
    res.send("Error: " + ex.message);
  }
});

router.get("/checkIfUrlExists", function (req, res) {
  try {
    const url = req.query.url;
    fetch(url)
      .then((response) => {
        if (response.status == 200) {
          res.send("ok");
        } else {
          res.send("failed");
        }
      })
      .catch((ex) => {
        res.send("failed: " + ex.message);
      });
  } catch (ex) {
    res.send("failed: " + ex.message);
  }
});

router.get("/profileImage", function (req, res) {
  try {
    let dir = __dirname + "/../Data/profileImage";
    var path = require("path");
    dir = path.resolve(dir);

    const memberId = req.query.memberId;
    var filename = `${dir}/${memberId}.jpg`;
    if (fs.existsSync(filename)) {
      res.sendFile(filename);
    } else {
      res.sendStatus(404);
    }
  } catch (ex) {
    res.send("failed: " + ex.message);
  }
});

router.post("/profileImage", function (req, res) {
  try {
    var data = req.body;

    var memberId = data.memberId;
    const img = data.img;

    const dir = `Data/profileImage`;
    if (!fs.existsSync(dir)) {
      fs.mkdirSync(dir);
    }

    var filename = `${dir}/${memberId}.jpg`;

    var base64Data = img.replace(/^data:image\/png;base64,/, "");
    fs.writeFileSync(filename, base64Data, "base64");

    res.send("ok");
  } catch (ex) {
    res.send("failed: " + ex.message);
  }
});

module.exports = router;
