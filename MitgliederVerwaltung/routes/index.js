﻿var express = require('express');
var router = express.Router();
var auth = require('../auth');
var moment = require('moment');

router.get('/', function (req, res) {
    res.render('index', req.query);
});

router.get('/verwaltung', function (req, res) {
    var loginfailed = req.query.loginfailed;
    loginfailed = (loginfailed == "");

    var url = req.query.url
    res.render('Verwaltung', {loginfailed: loginfailed, url: url});
});

router.post('/login', function (req, res) {
    var accessToken = global.login(req);
    if (accessToken) {

        var cookieDate = new Date(moment().add(2, 'hours').toDate());

        res.cookie('access-token', accessToken, {expires: cookieDate});

        var url = req.query.url
        if (!url || url == "undefined")
            url = '/members/list'

        if (req.body.login.toLowerCase() == "gastronomie")
            res.redirect(302, '/members/Verzehrumlage');
        else
            res.redirect(302, url) // '/members/list');



    } else {
        res.redirect(302, '/?loginfailed');
    }

});


module.exports = router;