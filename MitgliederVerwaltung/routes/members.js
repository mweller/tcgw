﻿"use strict";

var express = require("express");
var jade = require("jade");
var router = express.Router();
var fs = require("fs");
var moment = require("moment");
var iconv_lite = require("iconv-lite");
var request = require("sync-request");
// var ibantools = require('ibantools');

var mkdirSync = function (path) {
  try {
    fs.mkdirSync(path);
  } catch (e) {
    if (e.code != "EEXIST") {
      throw e;
    }
  }
};

var compileTemplateRechnung = null;

var dataPath = "Data/";

var Result = [];

var workbook = null;
var memberRowHeaders = [];
var members = null;
var masterMembersById = null;
var Gebuehren = {};
var TimeStampModified = "";

var StatusOptions = {
  "00": "?unbekannt?",
  VM: "Vollmitglied",
  FM: "Familienmitglied",
  J: "Jugendlicher",
  K: "Kind (bis 8 Jahre)",
  St: "Student",
  JT: "Schnupperkind",
  J3: "Jugendlicher(beitragsfrei)",
  E: "Eltern",
  FöM: "Fördermitglied",
  RM: "Ruhendes Mitglied",
};

var GESENDET = "(gesendet)";

function IsMobile(req) {
  var ua = req.headers["user-agent"];
  return ua.toLowerCase().indexOf("mobile") >= 0;
}

function getMemberById(id) {
  return members[id];
}

function familyMembers(masterMemberId) {
  if (masterMembersById[masterMemberId]) {
    return masterMembersById[masterMemberId].Items;
  } else {
    console.log("invalid MasterMemberID: " + masterMemberId);
    return [members[masterMemberId]];
  }
}

function formatIBAN(s) {
  return s.substr(0, 4) + " " + s.substr(4, 4) + " " + s.substr(8, 4) + " " + s.substr(12, 4) + " " + s.substr(16, 4) + " " + s.substr(20);
}

function formatIBANRechnung(s) {
  return s.substr(0, 4) + " " + "****" + " " + "****" + " " + "****" + " " + s.substr(16, 4) + " " + s.substr(20);
}

function readFileSync_encoding(filename, encoding) {
  var content = fs.readFileSync(filename);
  return iconv_lite.decode(content, encoding);
}

function writeFileSync_encoding(filename, encoding, s) {
  var content = iconv_lite.encode(s, encoding);
  fs.writeFileSync(filename, content);
}

function toAnsiBuffer(s) {
  return iconv_lite.encode(s, "windows1250");
}

function processHeaderLine(line) {
  memberRowHeaders = line.trim().split(";");
}

var MemberErrorFound = false;

function processDataLine(line) {
  line = line.trim();
  if (line == "") return;

  var fields = line.split(";");

  var member = { LastRowString: line };

  for (var i in fields) {
    fields[i] = csvDecode(fields[i].trim());

    if (fields[i] != "") member[memberRowHeaders[i]] = fields[i];
  }

  if (!member.MemberID) {
    // console.log("Invalid data line:" + line);
    return;
  }

  if (members[member.MemberID]) {
    var lastMember = members[member.MemberID];

    if ((member.FirstName == lastMember.FirstName && member.LastName == lastMember.LastName) || member.ChangeDate != undefined) {
      // console.log("duplicate Member ID ok: " + member.MemberID + ": " + member.FirstName + " " + member.LastName + " / " + lastMember.FirstName + " " + lastMember.LastName)
      // ok
    } else {
      console.log("duplicate Member ID: " + member.MemberID + ": " + member.FirstName + " " + member.LastName + " / " + lastMember.FirstName + " " + lastMember.LastName);

      var Remark = "";
      if (member.Remark) {
        Remark = "\n" + member.Remark;
      }

      member.Remark = "Mitgliedsnummer automatisch korrigiert alt: " + member.MemberID + Remark;

      member.MemberID = _newMemberID();
      MemberErrorFound = true;
    }
  }

  // fix mssing enter date
  if (members[member.MemberID]) {
    if (member.EnterDate == undefined && members[member.MemberID].EnterDate != undefined) {
      member.EnterDate = members[member.MemberID].EnterDate;
    }
  } else {
    if (member.ChangeDate != undefined && member.EnterDate == undefined && member.MemberID > 2163) {
      var d1 = moment(member.ChangeDate, "D.M.YYYY");
      if (d1.year() == 2017) {
        member.EnterDate = d1.format("DD.MM.YYYY");
        console.log("fixed enterDate for " + member.FirstName + " " + member.LastName);
      }
    }
  }

  // end of fix mssing enter date

  if (members[member.MemberID]) {
    var lastMember = members[member.MemberID];

    var ChangeHist = lastMember.ChangeHist;
    if (!ChangeHist) ChangeHist = [];

    for (var prop in member) {
      if (lastMember[prop] != member[prop] && prop != "ChangeDate" && prop != "LastRowString" && prop != "Verzehrumlage" && prop != "Umlage" && prop != "Age" && lastMember[prop]) {
        ChangeHist.push({ ChangeDate: member.ChangeDate, Property: prop, OldValue: lastMember[prop] });
        // console.log ("new val: " + member.MemberID + ":  " + prop + ": " + member[prop])
      }
    }

    member.ChangeHist = ChangeHist;
  }

  if (member.EnterDate) {
    var d1 = moment(member.EnterDate, "D.M.YYYY");
    member.yEnter = d1.year();
  }

  if (member.LeaveDate) {
    var d1 = moment(member.LeaveDate, "D.M.YYYY");
    member.yLeave = d1.year();
  }

  members[member.MemberID] = member;
}

function isMasterMember(member) {
  return member.MemberID == member.MasterMemberID;
}

function CheckMissingMasterMembers() {
  for (var i in members) {
    var MasterMemberID = members[i].MasterMemberID;
    var MasterMemberID_isValid = members[MasterMemberID] != undefined;

    if (MasterMemberID_isValid) {
      MasterMemberID_isValid = isMasterMember(members[MasterMemberID]);
    }

    if (!MasterMemberID_isValid) {
      // MasterMember existiert nicht ...
      // Fix: sich selbst zum masterMember machen

      console.log("Invalid MasterMember found: " + MasterMemberID + " " + members[i].FirstName + " " + members[i].LastName);
      MemberErrorFound = true;
      var oldMaster = "";
      var member = members[i];
      member.MasterMemberID = member.MemberID;

      SetChangeDate(member);

      var Remark = "";
      if (member.Remark) {
        Remark = "\n" + member.Remark;
      }

      member.Remark = "Hauptmitgliedsnummer automatisch korrigiert alt: " + oldMaster + Remark;
    }
  }
  // todo
}

function CheckInactiveMasterMembers() {
  for (var i in members) {
    var MasterMemberID = members[i].MasterMemberID;
    if (members[MasterMemberID] == undefined) {
      // MasterMember existiert nicht ... sollte hier nicht mehr vorkommen können
      // Fix: sich selbst zum masterMember machen
      MemberErrorFound = true;
      members[i].MasterMemberID = members[i].MemberID;
    } else {
      if (members[i].Aktiv != "false") {
        if (members[MasterMemberID].Aktiv == "false") {
          // remark feld fehlt .....
          console.log("passiver MasterMember found: " + MasterMemberID + " memberID=" + i);
        }
      }
    }
  }
  // todo
}

function loadPatch() {
  var filename = dataPath + "Patch.csv";

  if (!fs.existsSync(filename)) return false;

  var data = readFileSync_encoding(filename, "windows1250");
  var lines = data.split("\n");

  var RowHeaders = lines[0].trim().split(";");
  var MemberIDindex = -1;
  for (var i in RowHeaders) {
    if (RowHeaders[i] == "MemberID") {
      MemberIDindex = i;
      break;
    }
  }

  for (var l = 1; l < lines.length; l++) {
    var ss = lines[l].trim().split(";");

    var MemberID = ss[MemberIDindex];
    if (MemberID != "") {
      var member = members[MemberID];

      if (member == undefined) {
        console.log("Member to patch not found: " + MemberID);
      } else {
        var changed = false;
        for (var i in ss) {
          if (RowHeaders[i] != "MemberID") {
            ss[i] = ss[i].trim();
            if (ss[i] != "" && member[RowHeaders[i]] != ss[i]) {
              SetChangeDate(member);
              if (member.ChangeHist == undefined) member.ChangeHist = [];
              member.ChangeHist.push({ ChangeDate: member.ChangeDate, Property: RowHeaders[i], OldValue: member[RowHeaders[i]] });

              member[RowHeaders[i]] = ss[i];
              changed = true;
            }
          }
        }

        if (changed) saveAppendOneMember(member);
      }
    }
  }
}

function loadMembers(onDone) {
  var filename = dataPath + "Members.csv";

  var stats = fs.statSync(filename);
  if (stats.mtime.toString() == TimeStampModified.toString()) {
    if (onDone) onDone();
    return;
  }

  //
  // Jede Woche in der wir auf das System zugreifen
  // wird einmalig ein Backup erstellt
  //

  var backupWeek = moment(new Date()).format("YYYY-ww");
  var filenameBackup = dataPath + "backup/Members-" + backupWeek + ".csv";
  if (!fs.existsSync(filenameBackup)) {
    if (!fs.existsSync(dataPath + "backup")) {
      fs.mkdirSync(dataPath + "backup");
    }
    fs.copyFileSync(filename, filenameBackup);
  }

  TimeStampModified = stats.mtime;

  MemberErrorFound = false;

  members = [];

  var data = readFileSync_encoding(filename, "windows1250");
  var lines = data.split("\n");

  processHeaderLine(lines[0]);
  for (var i = 1; i < lines.length; i++) {
    processDataLine(lines[i]);
  }

  loadPatch();

  masterMembersById = [];

  var AktivMemberCnt = 0;

  for (var i in members) {
    var MasterMemberID = members[i].MasterMemberID;

    if (masterMembersById[MasterMemberID] == undefined) masterMembersById[MasterMemberID] = {};

    if (masterMembersById[MasterMemberID].Items == undefined) masterMembersById[MasterMemberID].Items = [];

    if (members[i].Aktiv != "false") {
      masterMembersById[MasterMemberID].Items.push(members[i]);
      console.log(members[i].Aktiv + " " + members[i].LastName);
      AktivMemberCnt++;
    }

    try {
      var d = moment(members[i].BirthDate, "D.M.YYYY");
      members[i].Age = moment().diff(d, "years");
      if (isNaN(members[i].Age)) members[i].Age = "??";
    } catch (ex) {
      members[i].Age = "??";
    }
  }

  console.log("Aktiv Members loaded: " + AktivMemberCnt);

  CheckMissingMasterMembers();

  if (MemberErrorFound) {
    // save all membes after an error ist found !!!!
    saveAllMembersConsolidated();
  }

  CheckInactiveMasterMembers();

  loadGebuehren();

  if (onDone) onDone();
}

function loadGebuehren() {
  var filename = dataPath + "Beitraege.csv";

  Gebuehren = {};

  var data = readFileSync_encoding(filename, "utf8");
  var ss = data.split("\n");

  for (var i = 1; i < ss.length; i++) {
    ss[i] = ss[i].trim();
    var sss = ss[i].split(";");
    Gebuehren[sss[0]] = {};
    Gebuehren[sss[0]].Description = sss[1];
    Gebuehren[sss[0]].Beitrag = sss[2];
    Gebuehren[sss[0]].Gastronomie = sss[3];
    Gebuehren[sss[0]].Umlage = sss[4];
  }
}

String.prototype.replaceAll = function (target, replacement) {
  return this.split(target).join(replacement);
};

function csvEncode(s) {
  try {
    var s1;
    do {
      s1 = s;
      if (s == undefined) return;

      if (typeof s === "number") return s.toString();

      s = s.replace(";", "%3B");
      s = s.replace("\n", "%0A");
      s = s.replace("\r", "%0C");
    } while (s1 != s);

    return s;
  } catch (ex) {
    console.log("error in csvEncode: " + ex.message);
  }
}

function csvDecode(s) {
  try {
    var s1;
    do {
      s1 = s;
      if (s == undefined) return;

      s = s.replace("%3B", ";");
      s = s.replace("%0A", "\n");
      s = s.replace("%0C", "\r");
    } while (s1 != s);

    return s;
  } catch (ex) {
    console.log("error in csvEncode: " + ex.message);
  }
}

function buildMemberRowString(member) {
  if (member == undefined) return;

  var s = "";
  for (var i in memberRowHeaders) {
    if (member[memberRowHeaders[i]]) {
      s += ";" + csvEncode(member[memberRowHeaders[i]]);
    } else {
      s += ";";
    }
  }

  return s.substr(1);
}

function saveAllMembersConsolidated() {
  var filename = dataPath + "Members.csv";

  var filenameBAK = dataPath + "Members" + moment(new Date()).format("-YYYY-MM-DD-") + "bak.csv";
  if (!fs.existsSync(filenameBAK)) fs.renameSync(filename, filenameBAK);

  var file = fs.createWriteStream(filename, { flags: "w" });

  var s = "";
  for (var i in memberRowHeaders) s += ";" + memberRowHeaders[i];

  file.write(toAnsiBuffer(s.substr(1) + "\n"));

  var check = [];
  var cnt = 0;

  for (var m in members) {
    var member = members[m];
    if (check[m.trim()]) console.log("SaveConsolidated:  doppelt: '" + m + "'  " + check[m.trim()]);
    check[m.trim()] = "X";
    file.write(toAnsiBuffer(buildMemberRowString(member) + "\n"));
    cnt++;
  }

  console.log("Saved Consolidated: #" + cnt);

  file.end();
}

function SetChangeDate(member) {
  member.ChangeDate = moment(new Date()).format("D.M.YYYY HH:mm");
}

function saveAppendOneMember(member) {
  if (member == undefined) return;

  var s = buildMemberRowString(member);
  if (member.LastRowString == s) {
    // no change ...
    return;
  }

  // Add the modification of of a member to the end of the csv-file
  // when reading it on load this line will overwite the previous line(s) for this member
  // so we will have allways the lates modification
  SetChangeDate(member);

  var filename = dataPath + "Members.csv";
  try {
    fs.appendFileSync(filename, toAnsiBuffer(buildMemberRowString(member) + "\n"));
  } catch (ex) {
    console.log("Error append to file: " + ex.message);
  }

  member.LastRowString = buildMemberRowString(member);

  console.log("Appended changed member: " + member.MemberID);
}

function _newMemberID() {
  var mxID = 2165;
  for (var i in members) {
    if (parseInt(members[i].MemberID) > mxID) mxID = parseInt(members[i].MemberID);
  }

  mxID++;
  return mxID;
}

function _createNewMember() {
  var mxID = _newMemberID();
  members[mxID.toString()] = {
    MemberID: mxID.toString(),
    MasterMemberID: mxID,
    FirstName: "-neu-",
    LastName: "-neu-",
    EnterDate: moment(new Date()).format("D.M.YYYY"),
  };

  masterMembersById[mxID.toString()] = { Items: [members[mxID.toString()]] };

  return mxID;
}

router.get("/new", function (req, res) {
  loadMembers(function () {
    var MemberID = _createNewMember();

    res.redirect("/members/list?new&id=" + MemberID);
  });
});

router.get("/setfield", function (req, res) {
  console.log("Setfield " + req.query.MemberID + "  " + req.query.FieldName + "  " + req.query.FieldValue);

  var member = members[req.query.MemberID];
  if (!member) {
    res.send("Member not found: " + req.query.MemberID);
    return;
  }

  member[req.query.FieldName] = req.query.FieldValue;
  saveAppendOneMember(member);
});

router.get("/editmember", function (req, res) {
  loadMembers(function () {
    var changed = false;
    var member = members[req.query.MemberID];
    for (var i in req.query) {
      if (member[i] != req.query[i]) {
        console.log("member field changed: " + i + "  " + req.query[i]);
        member[i] = req.query[i];
        changed = true;
      }
    }

    if (changed) {
      saveAppendOneMember(member);
      res.send("Changed");
    } else {
      res.send("Unchanged");
    }
  });
});

router.get("/Verzehrumlage", function (req, res) {
  loadMembers(function () {
    var rows = [];
    for (var i in members) {
      if (members[i].LastName == "Schumacher") {
        console.log("xxxx: " + members[i].LastName);
      }

      console.log("yyy: " + members[i].LastName);

      if (members[i].Aktiv != "false" && isMasterMember(members[i])) {
        rows.push(members[i]);
        console.log("zzz: " + members[i].LastName);
      }
    }

    res.render("Verzehrumlage", {
      rows: rows,
      familyMembers: familyMembers,
      isMasterMember: isMasterMember,
      StatusOptions: StatusOptions,
      IsMobile: IsMobile(req),
    });
  });
});

function _EnterLeaveOne(ea, LeaveDate, MemberStatus, Leave, LeaveBetrag) {
  var LeaveYear = undefined;

  if (LeaveDate && LeaveDate > "") LeaveYear = LeaveDate.split(".")[2];

  //
  // Vor 2013 haben wir keine Daten
  //
  if (!LeaveYear || LeaveYear < 2013) return;

  if (!Leave[LeaveYear]) {
    Leave[LeaveYear] = [];
    Leave[LeaveYear]["00"] = { Enter: 0, Leave: 0 };
  }

  if (!Leave[LeaveYear][MemberStatus]) Leave[LeaveYear][MemberStatus] = { Enter: 0, Leave: 0 };

  Leave[LeaveYear][MemberStatus][ea] = Leave[LeaveYear][MemberStatus][ea] + 1;

  Leave[LeaveYear]["00"][ea]++;

  if (!LeaveBetrag[LeaveYear]) LeaveBetrag[LeaveYear] = { Enter: 0, Leave: 0 };

  if (!Gebuehren[MemberStatus]) console.log("illegaler MemberStaus: " + MemberStatus);
  else LeaveBetrag[LeaveYear][ea] += parseInt(Gebuehren[MemberStatus].Beitrag);
}

router.get("/Statistik", function (req, res) {
  var rows = [];
  for (var i in members) {
    rows.push(members[i]);
  }

  loadMembers(function () {
    var EnterLeave = [];
    var EnterLeaveBetrag = [];

    for (var i in members) {
      var member = members[i];
      _EnterLeaveOne("Leave", members[i].LeaveDate, members[i].MemberStatus, EnterLeave, EnterLeaveBetrag);
      _EnterLeaveOne("Enter", members[i].EnterDate, members[i].MemberStatus, EnterLeave, EnterLeaveBetrag);
    }

    res.render("Statistik", {
      rows: rows,
      familyMembers: familyMembers,
      isMasterMember: isMasterMember,
      StatusOptions: StatusOptions,
      Beitraege: Gebuehren,
      EnterLeave: EnterLeave,
      EnterLeaveBetrag: EnterLeaveBetrag,
      IsMobile: IsMobile(req),
    });
  });
});

router.get("/list", function (req, res) {
  loadMembers(function () {
    var currentTag = req.query.tag;
    var ausgetreteneMitgliederAnzeigen = req.query.showExists != undefined;

    if (currentTag == undefined || currentTag == "") currentTag = null;
    else currentTag = "#" + currentTag;

    var specialColumns = [];
    if (currentTag != null) specialColumns.push({ Name: currentTag.substr(1), Description: currentTag.substr(1) });

    var b = 0;
    if (currentTag == "#Beitrag Training Sommer") b = Gebuehren.TrSommer.Beitrag;
    else if (currentTag == "#Beitrag Training Winter") b = Gebuehren.TrWinter.Beitrag;

    var allSpecialColumns = [];
    for (var i in memberRowHeaders) {
      var s = memberRowHeaders[i];
      if (s.substr(0, 1) == "#") allSpecialColumns.push({ Name: s.substr(1), Description: s.substr(1) });
    }

    var rows = [];
    for (var i in members) {
      if (ausgetreteneMitgliederAnzeigen || members[i].Aktiv != "false") rows.push(members[i]);
    }

    res.render("memberList", {
      rows: rows,
      additionalColumns: [],
      specialColumns: specialColumns,
      allSpecialColumns: allSpecialColumns,
      familyMembers: familyMembers,
      isMasterMember: isMasterMember,
      formatIBAN: formatIBAN,
      DefaultBeitrag: b,
      Beitraege: Gebuehren,
      StatusOptions: StatusOptions,
      showLeaveDate: ausgetreteneMitgliederAnzeigen,
      IsMobile: IsMobile(req),
    });

    //var compiledTemplate = jade.compileFile("views/memberList.jade");
    //var html = compiledTemplate(
    //     {
    //    rows: rows,
    //    specialColumns : specialColumns,
    //    allSpecialColumns: allSpecialColumns,
    //    familyMembers: familyMembers,
    //    formatIBAN: formatIBAN,
    //    DefaultBeitrag: b,
    //    showLeaveDate: ausgetreteneMitgliederAnzeigen
    //});

    //res.send(html);
  });
});

router.get("/listBeitrag", function (req, res) {
  loadMembers(function () {
    var ausgetreteneMitgliederAnzeigen = false;

    var additionalColumns = [];
    additionalColumns.push({ Name: "Beitrag", Description: "Beitrag" });
    additionalColumns.push({ Name: "Gastronomie", Description: "Gastronomie" });
    additionalColumns.push({ Name: "Umlage", Description: "Umlage" });

    additionalColumns.push({ Name: "#Beitrag Training Winter", Description: "Tr. Winter" });
    additionalColumns.push({ Name: "#Beitrag Training Sommer", Description: "Tr. Sommer" });

    var rows = [];
    for (var i in members) {
      if (members[i].Aktiv != "false") {
        rows.push(members[i]);
        var G = Gebuehren[members[i].MemberStatus];
        members[i].Gastronomie = G.Gastronomie;
        members[i].Umlage = G.Umlage;
        members[i].Beitrag = G.Beitrag;
      }
    }

    res.render("memberList", {
      rows: rows,
      additionalColumns: additionalColumns,
      specialColumns: [],
      allSpecialColumns: [],
      familyMembers: familyMembers,
      isMasterMember: isMasterMember,
      Beitraege: Gebuehren,
      StatusOptions: StatusOptions,
      showLeaveDate: ausgetreteneMitgliederAnzeigen,
      IsMobile: IsMobile(req),
    });

    //var compiledTemplate = jade.compileFile("views/memberList.jade");
    //var html = compiledTemplate(
    //     {
    //    rows: rows,
    //    specialColumns : specialColumns,
    //    allSpecialColumns: allSpecialColumns,
    //    familyMembers: familyMembers,
    //    formatIBAN: formatIBAN,
    //    DefaultBeitrag: b,
    //    showLeaveDate: ausgetreteneMitgliederAnzeigen
    //});

    //res.send(html);
  });
});

function GenerateTrainigsRechnungsDaten(memberId, rechnungsArt, Rechnung) {
  var RechnungsPositionen = [];
  var GesamtBetrag = 0;
  var pos = 0;

  if (members[memberId].MasterMemberID != memberId) {
    // ist keine MasterMember --> keine Rechnung produzieren !!!
    Rechnung.GesamtBetrag = 0;
    return;
  }

  var fMembers = familyMembers(memberId);

  if (fMembers == null) {
    // ist keine MasterMember (Some error ??) --> keine Rechnung produzieren !!!
    Rechnung.GesamtBetrag = 0;
    return;
  }

  for (var i in fMembers) {
    var betrag = fMembers[i]["#Beitrag Training " + rechnungsArt];
    if (betrag) {
      betrag = parseInt(betrag);
      RechnungsPositionen.push({ Pos: ++pos, Beschreibung: "Training " + fMembers[i].FirstName, Betrag: betrag });
      GesamtBetrag += betrag;
    }
  }

  members[memberId]["#Rechnung Training " + rechnungsArt] = GesamtBetrag;

  Rechnung.RechnungsPositionen = RechnungsPositionen;
  Rechnung.GesamtBetrag = GesamtBetrag;
}

function GenerateGastRechnungsDaten(memberId, rechnungsArt, Rechnung) {
  var RechnungsPositionen = [];
  var GesamtBetrag = 0;
  var pos = 0;

  var member = members[memberId];

  if (!member[rechnungsArt]) {
    // --> keine Rechnung produzieren !!!
    Rechnung.GesamtBetrag = 0;
    return;
  }

  var gastDaten = member.gastDaten;

  for (var i in gastDaten) {
    var gast1 = gastDaten[i];
    var doppel = gast1.doppel;
    if (doppel != "") {
      doppel = " (Doppel)";
    }
    const GebuehrGast = 10;
    RechnungsPositionen.push({ Beschreibung: gast1.gast + doppel, Beschreibung2: gast1.date, Betrag: GebuehrGast });
    GesamtBetrag += GebuehrGast;
  }

  Rechnung.RechnungsPositionen = RechnungsPositionen;
  Rechnung.GesamtBetrag = GesamtBetrag;
}

function istInDiesemJahrEingetreten(member) {
  if (member.EnterDate) {
    var d1 = moment(member.EnterDate, "D.M.YYYY");
    const currentYear = new Date().getFullYear();
    if (d1.year() == currentYear) {
      return true;
    }
  }
  return false;
}

function GenerateBeitragsRechnungsDaten(memberId, Rechnung) {
  var RechnungsPositionen = [];
  var GesamtBetrag = 0;
  var GesamtBetragVerzehr = 0;
  var GesamtUmlage = 0;
  var pos = 0;

  var member = members[memberId];

  if (member.MasterMemberID != memberId || member.Status == "JT" || member.Status == "J3") {
    // ist keine MasterMember --> keine Rechnung produzieren !!!
    Rechnung.GesamtBetrag = 0;
    Rechnung.GesamtBetragVerzehr = 0;
    return;
  }

  var fMembers = familyMembers(memberId);

  if (fMembers == null) {
    // ist keine MasterMember (Some error ??) --> keine Rechnung produzieren !!!
    Rechnung.GesamtBetrag = 0;
    return;
  }

  for (var i in fMembers) {
    var memberStatus = fMembers[i].MemberStatus;

    var Gebuehr = Gebuehren[memberStatus];
    var GebuehrNeuaufnahme = Gebuehren[memberStatus + "-neu"];

    // // Zur Zeit keine Rabatte für Neumitglieder MWE 10.2.2024
    // //
    // // Mitglieder die in diesem Jahr eingetreten sind werden bei der normalen Rechnungstellung gesondert behandelt,
    // // da für sie regelmaässig Sonderkonditionen gelten
    // //
    // if (istInDiesemJahrEingetreten(fMembers[i])) {
    //   if (GebuehrNeuaufnahme) {
    //     Gebuehr = GebuehrNeuaufnahme;
    //   }
    // }

    var betrag;
    if (Gebuehr) betrag = Gebuehr.Beitrag;

    // Mitgliedsbeitrag
    if (betrag) {
      betrag = parseInt(betrag);
      RechnungsPositionen.push({ Pos: ++pos, Beschreibung: fMembers[i].FirstName + " (" + Gebuehr.Description + ")", Betrag: betrag });
      GesamtBetrag += betrag;
    } else {
      console.log("Keine Gebühr gefunden für MemberStatus: " + fMembers[i].MemberStatus);
    }

    // einmalige Mitgliedsbeitrag
    if (Gebuehr) betrag = Gebuehr.Umlage;

    if (betrag) {
      betrag = parseInt(betrag);
      if (betrag > 0) {
        RechnungsPositionen.push({ Pos: ++pos, Tab: true, Beschreibung: "Einmalige Umlage", Betrag: betrag });
        GesamtBetrag += betrag;
        GesamtUmlage += betrag;
      }
    } else {
      console.log("Keine Umlage für MemberStatus: " + fMembers[i].MemberStatus);
    }

    // Verzehrumlage
    var betragVerzehr;
    if (Gebuehr) betragVerzehr = Gebuehr.Gastronomie;

    if (betragVerzehr && betragVerzehr > 0) {
      betragVerzehr = parseInt(betragVerzehr);
      RechnungsPositionen.push({ Pos: ++pos, Tab: true, Beschreibung: "Verzehrumlage", Betrag: betragVerzehr });
      GesamtBetragVerzehr += betragVerzehr;
      GesamtBetrag += betragVerzehr;
    } else {
      console.log("Keine Gebühr gefunden für MemberStatus: " + fMembers[i].MemberStatus);
    }
  }

  if (GesamtBetragVerzehr > 122) {
    var delta = 122 - GesamtBetragVerzehr;
    RechnungsPositionen.push({ Pos: ++pos, Beschreibung: "Gutschrift wg. Begrenzung Verzehrumlage ", Betrag: delta });
    GesamtBetrag += delta;
    GesamtBetragVerzehr += delta;
  }

  if (GesamtBetrag > 0 && !member.AccountIBAN) {
    RechnungsPositionen.push({ Pos: ++pos, Beschreibung: "Gebühr fehlende Einzugsermächtigung ", Betrag: 10 });
    GesamtBetrag += 10;
  }

  member["#Rechnung Mitgliedschaft"] = GesamtBetrag;
  member["Verzehrumlage"] = GesamtBetragVerzehr;
  member["Umlage"] = GesamtUmlage;

  Rechnung.RechnungsPositionen = RechnungsPositionen;
  Rechnung.GesamtBetrag = GesamtBetrag;
  Rechnung.GesamtBetragVerzehr = GesamtBetragVerzehr;
}

function GeneratePinDaten(memberId, Rechnung) {
  var RechnungsPositionen = [];
  var pos = 0;
  var fMembers = familyMembers(memberId);
  if (fMembers == null) {
    // ist keine MasterMember (Some error ??) --> keine Rechnung produzieren !!!
    Rechnung.GesamtBetrag = 0;
    return;
  }

  const currentYear = new Date().getFullYear();
  for (var i in fMembers) {
    var member = fMembers[i];
    var pin = (member.MemberID * 1234) & (9765 * currentYear) & 9341;
    RechnungsPositionen.push({ Pos: ++pos, Beschreibung: member.FirstName + " " + member.LastName, MitgliedsNummer: member.MemberID, Pin: pin });
  }

  Rechnung.RechnungsPositionen = RechnungsPositionen;
}

function GenerateRechnung(memberId, rechnungsArt) {
  var Member = getMemberById(memberId);
  if (Member == null) {
    return null;
  }

  if (Member.Aktiv == "false") {
    return null;
  }

  var Rechnung = {};

  var Title = "";
  if (Member.Title) Title = Member.Title + " ";

  Rechnung.Adresse1 = Title + Member.FirstName + " " + Member.LastName;
  Rechnung.Adresse2 = Member.Street;
  Rechnung.Adresse3 = Member.PostCode + " " + Member.City;

  const currentYear = new Date().getFullYear();

  Rechnung.Date = moment(new Date()).format("D.M.YYYY");
  switch (rechnungsArt) {
    case "Rechnung Training Winter":
      Rechnung.Betreff = "Rechnung Tennistraing Winter " + (currentYear - 1) + "/" + currentYear;
      GenerateTrainigsRechnungsDaten(memberId, "Winter", Rechnung);
      break;

    case "Rechnung Training Sommer":
      Rechnung.Betreff = "Rechnung Tennistraing Sommer " + currentYear;
      GenerateTrainigsRechnungsDaten(memberId, "Sommer", Rechnung);
      break;

    case "Rechnung Mitgliedschaft":
      Rechnung.Betreff = "Rechnung Mitgliedsbeitrag für " + currentYear;
      GenerateBeitragsRechnungsDaten(memberId, Rechnung);
      break;

    case "PinBrief":
      Rechnung.Betreff = "Zugangsdaten für den Clubpass " + currentYear;
      GeneratePinDaten(memberId, Rechnung);
      break;

    default:
      Rechnung.Betreff = rechnungsArt;
      GenerateGastRechnungsDaten(memberId, "#" + rechnungsArt, Rechnung);
      break;
  }

  if (Rechnung.GesamtBetrag == 0) return null;

  if (Member.Sex == "w") {
    Rechnung.Anrede = "Liebe ";
  } else {
    Rechnung.Anrede = "Lieber ";
  }

  Rechnung.Anrede += Member.FirstName + " " + Member.LastName;
  Member.AccountInhaber = Member.FirstName + " " + Member.LastName;
  if (Member.AccountLastName) {
    Member.AccountInhaber = Member.AccountFirstName + " " + Member.AccountLastName;
  }

  var locals = {
    Rechnung: Rechnung,
    Member: Member,
    formatIBAN: formatIBAN,
    formatIBANRechnung: formatIBANRechnung,
  };

  console.log("Render Rechnung");
  // var html = jade.renderFile("views/Rechnung.jade", locals);

  var html = compileTemplateRechnung(locals);

  console.log("Render Rechnung done");

  return html;
}

function _RechnungsFolder(RechnungsType) {
  const currentYear = new Date().getFullYear();

  return dataPath + currentYear + "/" + RechnungsType;
}

function _RechnungsFilename(memberId, RechnungsType) {
  var folder = _RechnungsFolder(RechnungsType);

  var member = getMemberById(memberId);

  var fullFileName = folder + "/" + member.MemberID + " - " + member.FirstName + " " + member.LastName + ".html";

  return fullFileName;
}

function _RechnungsFilenameGesendet(memberId, RechnungsType) {
  var fn = _RechnungsFilename(memberId, RechnungsType);
  return fn.split(".html") + " " + GESENDET + ".html";
}

function CreateAndSaveRechnung(RechnungsType, memberId) {
  try {
    var html = GenerateRechnung(memberId, RechnungsType);

    if (html == null) {
      return null;
    }

    var folder = _RechnungsFolder(RechnungsType);
    var member = getMemberById(memberId);

    var fn = _RechnungsFilename(memberId, RechnungsType);

    var p1 = folder.lastIndexOf("/");
    var f1 = folder.substr(0, p1);

    mkdirSync(f1); // "/2017", "/2018" usw
    mkdirSync(folder); // "Rechnungen Training Winter"

    fs.writeFileSync(fn, html);
    console.log("rechnung gespeichert: " + folder + "/" + fn);

    return html;
  } catch (ex) {
    console.log("Error in CreateAndSaveRechnung: " + ex.message);
  }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function removeBlanks(value) {
  var v = value;
  do {
    v = value;
    value = value.replace(" ", "");
  } while (v != value);

  return value;
}

function deUmlaut(value) {
  var v = value;
  do {
    v = value;
    value = value.replace(/Ä/g, "Ae");
    value = value.replace(/Ö/g, "Oe");
    value = value.replace(/Ü/g, "Ue");

    value = value.replace(/ä/g, "ae");
    value = value.replace(/ö/g, "oe");
    value = value.replace(/ü/g, "ue");

    value = value.replace(/ß/g, "ss");

    value = value.replace(/é/g, "e");
    value = value.replace(/è/g, "e");
  } while (v != value);

  var res = "";
  for (var i = 0; i < value.length; i++) {
    var s1 = value.substr(i, 1);
    if (s1 > "z") {
      s1 = "_";
    }
    res += s1;
  }

  return res;
}

function checkIBAN(IBAN_NUMBER, BIC) {
  // return true;

  // https://openiban.com/validate/ is offline wg DSGVO !!!!!
  // läuft wieder 10.10.2019

  var url = "https://openiban.com/validate/" + IBAN_NUMBER + "?getBIC=true&validateBankCode=true";

  var res = request("GET", url);

  var data = JSON.parse(res.getBody("utf8"));

  if (data.valid) {
    if (BIC != data.bankData.bic) {
      return {
        ok: false,
        bic: data.bankData.bic,
      };
    } else {
      return {
        ok: true,
      };
    }
  } else {
    return {
      ok: false,
      error: "IBAN falsch",
    };
  }
}

function createSepa(RechnungsType) {
  var allErrors = "";

  var SEPA = require("sepa");

  var doc = new SEPA.Document("pain.008.001.02");
  doc.grpHdr.id = "XMPL." + 20140201 + ".TR0";
  doc.grpHdr.created = new Date();
  doc.grpHdr.initiatorName = "TCGW Am Kreuzberg e.V.";

  var info = doc.createPaymentInfo();

  var d = moment().add(7, "days");

  info.collectionDate = d.toDate();
  info.creditorIBAN = "DE48370501980038700126";
  info.creditorBIC = "COLSDE33XXX";
  info.creditorName = "TCGW Am Kreuzberg e.V.";
  info.creditorId = "DE57ZZZ00000433988";

  doc.addPaymentInfo(info);

  var GesamtFieldName = "#" + RechnungsType;

  for (var m in members) {
    var member = members[m];

    try {
      if (isMasterMember(member) && member.Aktiv != "false" && member[GesamtFieldName] != undefined && member.AccountIBAN != undefined) {
        var errorText = null;
        var tx = info.createTransaction();

        tx.debtorName = deUmlaut(member.FirstName + " " + member.LastName);
        tx.debtorIBAN = removeBlanks(member.AccountIBAN.toUpperCase());
        if (member.AccountBIC) tx.debtorBIC = removeBlanks(member.AccountBIC.toUpperCase());
        else errorText = "Keine BIC";

        tx.mandateId = "MitgliedsNr " + member.MemberID;
        tx.mandateSignatureDate = new Date("2017-01-01");
        tx.amount = parseInt(member[GesamtFieldName]);
        tx.remittanceInfo = RechnungsType;
        tx.end2endId = "Der TCGW bedankt sich";

        try {
          tx.validate();
        } catch (err) {
          errorText = err.message;
        }

        if (!errorText && tx.amount > 0 && SEPA.validateIBAN(tx.debtorIBAN)) {
          info.addTransaction(tx);
        } else {
          if (allErrors == "") allErrors = "Illegale Einzugseintrage: <br/>";

          allErrors += tx.debtorName + ": " + tx.debtorIBAN + " " + tx.debtorBIC + " " + errorText + "<br/>";
          console.log("illegaler Einzugseintrag für Mitglied Nr " + m + ": " + errorText);
        }
      }
    } catch (ex) {
      allErrors += "Exception: " + member.FirstName + " " + member.LastName + " : " + ex.message + "<br/>";
    }
  }

  return { allErrors: allErrors, doc: doc.toString() };
}

router.get("/SEPA", function (req, res) {
  var RechnungsType = req.query.type; // "Rechnung Training Winter";

  const { allErrors, doc } = createSepa(RechnungsType);

  if (allErrors != "") {
    res.send(allErrors);
    return;
  } else {
    var headers = {
      "Content-Type": "application/force-download",
      "Content-disposition": "attachment; filename=SEPA " + RechnungsType + moment(new Date()).format("-YYYY-MM-DD") + ".xml",
    };

    res.writeHead(200, headers);
    res.end(doc);
  }
});

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// http://localhost:8089/members/rechnungenerstellen?type=PinBrief
router.get("/rechnungenerstellen", function (req, res) {
  rechnungenerstellen(req, res);
});

function rechnungenerstellen(req, res) {
  compileRechnung(req.query.type);
  xxErstellen(req, res, compileTemplateRechnung);
}

function xxErstellen(req, res, compileTemplateRechnung) {
  try {
    loadMembers(function () {
      try {
        var memberId = req.query.id;
        var RechnungsType = req.query.type; // "Training Winter";

        var generatedForMembers = [];

        var GesamtFieldName = "#" + RechnungsType;

        var mmm = members;
        if (memberId) {
          mmm = [];
          mmm[memberId] = members[memberId];

          //
          // EINE Rechnungen werden neu erstellt.
          // daher nur diese Rechnung löschen
          //
          var fn = _RechnungsFilename(memberId, RechnungsType);
          try {
            fs.unlinkSync(fn);
            fn = _RechnungsFilenameGesendet(memberId, RechnungsType);
            fs.unlinkSync(fn);
          } catch (ex) {
            console.log("keine Rechnung zum löschen gefunden: " + fn);
          }
        } else {
          //
          // ALLE Rechnungen werden neu erstellt.
          // daher ALLE vorhanden Rechnung löschen
          //
          try {
            var folder = _RechnungsFolder(RechnungsType);
            var files = fs.readdirSync(folder);
            for (var i in files) fs.unlinkSync(folder + "/" + files[i]);
          } catch (ex) {
            console.log("keine Rechnung zum löschen gefunden: " + fn);
          }
        }

        for (var m in mmm) {
          var member = members[m];

          //
          // Nur für Hauptmitglieder werden Rechnungen erzeugt
          //
          var lastGesamt = member[GesamtFieldName];

          var RechnungCreated = false;
          if (member.MemberID == member.MasterMemberID && member.Aktiv != "false") {
            var html = CreateAndSaveRechnung(RechnungsType, member.MemberID);
            if (html) {
              //
              // Save GesamtBetrag to File
              //
              RechnungCreated = true;
              saveAppendOneMember(member);
              //
              // resultlist ...
              //
              generatedForMembers.push({ Member: member, Status: "OK" });
            } else console.log("skip: " + member.FirstName + " " + member.LastName);
          } else {
            if (member[GesamtFieldName]) {
              member[GesamtFieldName] = undefined;
              saveAppendOneMember(member);
            }
          }

          if (member[GesamtFieldName] == "0") {
            member[GesamtFieldName] = undefined;
            saveAppendOneMember(member);
          }

          if (!RechnungCreated) {
            if (lastGesamt != member[GesamtFieldName]) {
              member[GesamtFieldName] = undefined;
              saveAppendOneMember(member);
            }
          }
        }

        //
        // Ergebnissliste renderen
        //

        if (req.query.noresultlist) {
        } else {
          res.render("membersResult", { Title: "Rechnungen wurden erzeugt für: " + RechnungsType, Result: generatedForMembers, RechnungsType: RechnungsType });
        }
      } catch (ex) {
        res.send("Error: " + ex.message);
      }
    });
  } catch (ex) {
    res.send("Error: " + ex.message);
  }
}

router.get("/printrechnungenohneemailadresse", function (req, res) {
  var htmlGesamt = "";
  var RechnungsType = req.query.type; // "Rechnung Training Winter";

  try {
    for (var memberId in members) {
      var member = members[memberId];
      if (isMasterMember(member) && member.Email == undefined) {
        var html = readRechnung(memberId, RechnungsType);
        if (html != null) htmlGesamt += html;
      }
    }
  } catch (err) {
    console.log("Error in PrintRechnungenOhneEmailAdresse: " + err.message);
  }

  res.send(htmlGesamt);
});

function _SendOneMail(memberId, RechnungsType, ResultList) {
  try {
    var html = readRechnung(memberId, RechnungsType);
    var member = members[memberId];

    if (!member.Email) return false;

    var toMail = member.Email;

    if (html == null) {
      ResultList.push({ Index: pendingMailProcess.Current, Member: members[memberId], Info: "Rechnung existiert nicht: " + RechnungsType });
      return;
    } else {
      var nodemailer = require("nodemailer");
      var smtpTransport = require("nodemailer-smtp-transport");

      var transporter = nodemailer.createTransport({
        host: "mail.incom.de",
        port: 25, // 465,
        secure: false, // true,
        auth: {
          user: "tcgw",
          pass: "Admin123!",
        },
        tls: {
          rejectUnauthorized: false,
        },
      });

      //   var transporter1und1 = nodemailer.createTransport(
      //     smtpTransport({
      //       service: "1und1",
      //       auth: {
      //         user: "noreply@tc-kreuzberg.de",
      //         pass: "tiebreak-mail",
      //       },
      //       tls: {
      //         rejectUnauthorized: false,
      //       },
      //     })
      //   );

      var fromMail = "Verwaltung TC GW Am Kreuzberg e.V.<club@tc-kreuzberg.de>";
      var subject = "TCGW Rechnung: " + RechnungsType;
      var bcc = "";

      ///////////s//////////////////////////////////////////////////////
      //  TestBetrieb für den MailVersand hier ein und ausschalten !!!
      /////////////////////////////////////////////////////////////////
      var testBetrieb = false;
      /////////////////////////////////////////////////////////////////

      if (testBetrieb) {
        toMail = "mail@michael-weller.de";

        // Teste Fehler im Namen:
        // Antwort mail an club@tc-kreuzberg.de kommt mit Fehlerangabe
        //

        // toMail = "mail@x-michael-weller.de";

        // Teste Fehler im ServerName:
        // Fehler beim senden kommt direkt als Rückmeldung in die Resultat Liste
        //

        // toMail = "x-mail@michael-weller.de";

        subject = "TEST: " + member.Email + " " + subject;
        fromMail = "Verwaltung TC GW Am Kreuzberg e.V.<mail@michael-weller.de>";
      }

      var testBetriebMitBcc = false;
      if (!testBetrieb || testBetriebMitBcc) {
        if (RechnungsType.indexOf("Training") >= 0) bcc += "jugend@tc-kreuzberg.de" + ";";

        bcc += "club@tc-kreuzberg.de" + ";";
        bcc += "finanzen@tc-kreuzberg.de" + ";";
        // bcc += 'mail@michael-weller.de';
      }

      transporter.sendMail(
        {
          from: fromMail,
          to: toMail,
          bcc: bcc,
          subject: subject,
          html: html,
        },
        function (error, info) {
          try {
            pendingMailProcess.Current++;

            if (error) {
              ResultList.push({ Index: pendingMailProcess.Current, Member: member, Info: "ERROR: " + error });
              console.log("Mail send ERROR: " + toMail + "  " + subject + "  " + error);
            } else {
              ResultList.push({ Index: pendingMailProcess.Current, Member: member, Info: member.Email + " OK: Email gesendet" });
              console.log("Mail send OK: " + toMail + "  " + subject + "  " + info.response);

              var fn = _RechnungsFilename(memberId, RechnungsType);
              var fn1 = _RechnungsFilenameGesendet(memberId, RechnungsType);

              fs.renameSync(fn, fn1);
            }
          } catch (ex) {
            console.log("Error in _sendOneMail, transporter.sendMail, callback: " + ex.message);
          }
        }
      );
    }
  } catch (ex) {
    console.log("Error in _sendOneMail: " + ex.message);
  }
}

var pendingMailProcess = null;

router.get("/getresultlist", function (req, res) {
  if (!pendingMailProcess.Title) {
    pendingMailProcess.Title = "Rechnungensversand für: " + pendingMailProcess.RechnungsType;
  }

  try {
    res.render("membersResult", {
      Title: pendingMailProcess.Title,
      Result: pendingMailProcess.ResultList,
      RechnungsType: pendingMailProcess.RechnungsType,
      pendingMailProcess: pendingMailProcess,
    });
  } catch (ex) {
    res.send("Error: " + ex.message);
  }
});

//
// Sende eine (query param id != null) oder alle Rechnungen
// die als File im Rechnungsfolder gespeichert sind.
//
router.get("/sendRechnungen", function (req, res) {
  sendRechnungen(req, res);
});

function sendRechnungen(req, res) {
  try {
    loadMembers(function () {
      try {
        var memberId = req.query.id;
        var RechnungsType = req.query.type; // "Rechnung Training Winter";

        var memberList = [];

        pendingMailProcess = {
          ResultList: [],
          Current: 0,
          Total: memberList.length,
          RechnungsType: RechnungsType,
        };

        if (memberId != undefined) {
          var member = members[memberId];
          memberList.push(member);
        } else {
          var folder = _RechnungsFolder(RechnungsType);
          var files = fs.readdirSync(folder);
          for (var i in files) {
            var filename = files[i];
            if (filename.indexOf(GESENDET) < 0) {
              filename = filename.split(" -")[0];
              memberId = parseInt(filename);
              var member = members[memberId];

              if (member.Email != undefined) memberList.push(member);
              else {
                pendingMailProcess.ResultList.push({ Index: pendingMailProcess.Current, Member: member, Info: "ERROR: Email Adresse fehlt" });
                pendingMailProcess.Current++;
              }
            }
          }
        }

        pendingMailProcess.Total = memberList.length;

        var oldLength = pendingMailProcess.ResultList.length;
        var cnt = 0;
        var first = true;

        var waiting = setInterval(function () {
          if (first || oldLength != pendingMailProcess.ResultList.length) {
            first = false;
            cnt = 0;

            if (memberList.length > 0) {
              var member = memberList[0];
              memberList.shift();
              oldLength = pendingMailProcess.ResultList.length;

              _SendOneMail(member.MemberID, RechnungsType, pendingMailProcess.ResultList);
            } else {
              clearInterval(waiting);
            }
          } else {
            cnt++;
            if (cnt > 1000) {
              // 50 sec timeout reached
              var member = memberList[0];
              memberList.shift();
              oldLength = pendingMailProcess.ResultList.length;

              pendingMailProcess.ResultList.push({ Index: pendingMailProcess.Current, Member: member, Info: "ERROR: Timeout beim senden" });
              pendingMailProcess.Current++;
            }
          }
        }, 50);

        if (req.query.noredirect) {
          res.send("@ok"); // zZ für selfservice PinBrift genutzt
        } else {
          res.redirect("/members/getresultlist");
        }
      } catch (ex) {
        console.log("Error: " + ex.message);
        res.send("Error: " + ex.message);
      }
    });
  } catch (ex) {
    res.send("Error: " + ex.message);
  }
}

router.get("/deleteRechnungen", function (req, res) {
  try {
    loadMembers(function () {
      try {
        var memberId = req.query.id;
        var RechnungsType = req.query.type; // "Rechnung Training Winter";

        var member = members[memberId];
        member["#" + RechnungsType] = "";
        saveAppendOneMember(member);

        var folder = _RechnungsFolder(RechnungsType);
        var files = fs.readdirSync(folder);
        for (var i in files) {
          var filename = files[i];
          if (filename.indexOf(GESENDET) < 0) {
            // eigentlich sollte man versendete Rechnungen nicht mehr löschen können !!!!
            // oder ??
            //
          }

          var s = filename.split(" -")[0];
          var mId = parseInt(s);

          var fullFilename = folder + "/" + filename;

          if (mId == memberId) {
            fs.unlink(fullFilename, function (err, result) {
              if (err) {
                console.log("Error delete  " + fullFilename + " : " + err);
                res.send("Error: " + err);
              } else {
                res.send("Ok, Rechnung wurde gelöscht");
              }
            });
          }
        }
      } catch (ex) {
        console.log("Error: " + ex.message);
        res.send("Error: " + ex.message);
      }
    });
  } catch (ex) {
    res.send("Error: " + ex.message);
  }
});

function readRechnung(memberId, RechnungsType) {
  var fn = _RechnungsFilename(memberId, RechnungsType);
  if (fs.existsSync(fn)) {
    return fs.readFileSync(fn, "utf-8");
  }

  fn = _RechnungsFilenameGesendet(memberId, RechnungsType);
  if (fs.existsSync(fn)) {
    return fs.readFileSync(fn, "utf-8");
  }

  return null;
}

router.get("/showRechnung", function (req, res) {
  loadMembers(function () {
    var memberId = req.query.id;
    var RechnungsType = req.query.type; // "Training Winter";

    var html = readRechnung(memberId, RechnungsType, true);

    if (html == null) res.send("Rechnung existiert nicht: " + RechnungsType + "; " + memberId);
    else {
      res.send(html);
    }
  });
});

//
// Alter am 1.1. diesen Jahres
//
function Age11(member) {
  const currentYear = new Date().getFullYear();

  var d1 = moment(member.BirthDate, "D.M.YYYY");
  var d2 = moment("1.1." + currentYear, "D.M.YYYY");

  return d2.diff(d1, "years");
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function pruefenGeburtstag(member, Result) {
  var d = moment(member.BirthDate, "D.M.YYYY");
  var Age = moment().diff(d, "years");
  if (isNaN(Age)) Result.push({ Member: member, Info: "kein oder falscher Geburtstag: " + member.BirthDate });
}

function pruefenStatusAlterJugendliche(member, Result) {
  if (Age11(member) >= 19 && member.MemberStatus == "J") Result.push({ Member: member, Info: "Status Jugendlicher, 19 Jahre oder älter am 1.1.: " + member.BirthDate });
}

function pruefenStatusAlterStudent(member, Result) {
  if (Age11(member) >= 29 && member.MemberStatus == "St") Result.push({ Member: member, Info: "Status Student, 29 Jahre oder älter am 1.1: " + member.BirthDate });
}

function pruefenStatusSchnupperKind(member, Result) {
  const currentYear = new Date().getFullYear();

  if (member.EnterDate && member.MemberStatus == "JT") {
    var d1 = moment(member.EnterDate, "D.M.YYYY");
    if (d1.year() < currentYear) Result.push({ Member: member, Info: "Schnupperkind müsste Mitglied werden. Eintritt: " + member.EnterDate });
  }
}

function pruefenFamilie(member, Result) {
  if (isMasterMember(member)) {
    if (member.MemberStatus == "FM") {
      Result.push({ Member: member, Info: "Familienmitglied als Hauptmitglied: " });
    }

    // Anzahl Familienmitglieder prüfen
    var anz = 0;
    var fMembers = familyMembers(memberId);
    if (fMembers == null) {
      for (var i in fMembers) {
        var fMember = fMembers[i];
        if (fMember.Status == "FM") {
          anz++;
        }
      }
    }

    if (anz > 1) {
      Result.push({ Member: member, Info: "Mehr als 1 Familienmitglied" });
    }
  }
}

function pruefenIBAN(member, Result) {
  if (isMasterMember(member)) {
    if (member.AccountIBAN && member.AccountIBAN != "") {
      var s = "Check IBAN für : " + member.FirstName + "  " + member.LastName + "  IBAN: " + member.AccountIBAN + " BIC: " + member.AccountBIC;

      var res = checkIBAN(member.AccountIBAN, member.AccountBIC);

      if (!res.ok) {
        console.log(s + " FAILED");
        if (res.bic) {
          member.AccountBIC = res.bic;
          saveAppendOneMember(member);
          Result.push({ Member: member, Info: "BIC war falsch, wurde automatisch korrigiert" });
        } else {
          Result.push({ Member: member, Info: "IBAN / BIC falsch " + "IBAN: " + member.AccountIBAN + " BIC: " + member.AccountBIC });
        }
      } else {
        console.log(s + " ok");
      }
    }
  }
}

router.get("/pruefen", function (req, res) {
  try {
    loadMembers(function () {
      try {
        var cnt = 0;
        var cntProcessed = 0;
        for (var i in members) {
          if (members[i] != null) cnt++;
        }

        pendingMailProcess = {
          ResultList: [],
          Current: 0,
          Total: cnt,
          Title: "Prüfung aller Mitglieder",
          RechnungsType: "",
        };

        //for (var i in members) {

        var i = 0;
        var waiting = setInterval(function () {
          var member = null;
          do {
            member = members[i];
            i++;
            pendingMailProcess.Current = i;
          } while (member == null && i < members.length);

          cntProcessed++;
          pendingMailProcess.Current = cntProcessed;

          if (cntProcessed >= cnt) {
            clearInterval(waiting);
          } else {
            if (member) {
              // pendingMailProcess.ResultList.push({Member: member, Info: "Prüfung läuft ..."});

              if (member.Aktiv != "false") {
                try {
                  pruefenGeburtstag(member, pendingMailProcess.ResultList);
                  pruefenStatusAlterJugendliche(member, pendingMailProcess.ResultList);
                  pruefenStatusAlterStudent(member, pendingMailProcess.ResultList);
                  pruefenStatusSchnupperKind(member, pendingMailProcess.ResultList);
                  pruefenFamilie(member, pendingMailProcess.ResultList);
                  pruefenIBAN(member, pendingMailProcess.ResultList);
                } catch (ex) {
                  console.log(ex.message);
                }
              }
            }
          }
        }, 10);

        //
        // Ergebnissliste renderen
        //

        res.redirect("/members/getresultlist");

        // res.render("membersResult", {Title: "Prüfungs Ergebnis: ", Result: Result});
      } catch (ex) {
        res.send("Error: " + ex.message);
      }
    });
  } catch (ex) {
    res.send("Error: " + ex.message);
  }
});

router.get("/adressListe", function (req, res) {
  try {
    loadMembers(function () {
      try {
        var Result = [];

        // mwe1
        var result = "\ufeff"; // String.fromCharCode(239) + String.fromCharCode(187) + String.fromCharCode(191); // UTF8 BOM

        result += "Aktiv;Title;LastName;FirstName;Email;PostCode;City;Street;MemberStatus;Sex;BirthDate;EnterDate;LeaveDate;MasterMemberID;MemberID\r\n";

        for (var i in members) {
          var member = members[i];
          if (isMasterMember(member)) {
            var s =
              member.Aktiv +
              ";" +
              member.Title +
              ";" +
              member.LastName +
              ";" +
              member.FirstName +
              ";" +
              member.Email +
              ";" +
              member.PostCode +
              ";" +
              member.City +
              ";" +
              member.Street +
              ";" +
              member.MemberStatus +
              ";" +
              member.Sex +
              ";" +
              member.BirthDate +
              ";" +
              member.EnterDate +
              ";" +
              member.LeaveDate +
              ";" +
              member.MasterMemberID +
              ";" +
              member.MemberID +
              "\r\n";
            s = s.replaceAll("undefined", "");
            result += s;
          }
        }

        //
        // Ergebnissliste renderen
        //

        var headers = {
          "Content-Type": "application/force-download",
          "Content-disposition": "attachment; filename=Adressliste TCGW " + moment(new Date()).format("-YYYY-MM-DD") + ".csv",
        };

        res.writeHead(200, headers);
        res.end(result);
      } catch (ex) {
        res.send("Error: " + ex.message);
      }
    });
  } catch (ex) {
    res.send("Error: " + ex.message);
  }
});

function getMembers() {
  if (members == null) {
    loadMembers();
  }
  return members;
}

function compileRechnung(rechnungsType) {
  if (rechnungsType.indexOf("Pin") < 0) {
    compileTemplateRechnung = jade.compileFile("views/Rechnung.jade");
  } else {
    compileTemplateRechnung = jade.compileFile("views/PinBrief.jade");
  }
}

module.exports = {
  router: router,
  getMembers: getMembers,
  saveAppendOneMember: saveAppendOneMember,
  createNewMember: _createNewMember,
  CreateAndSaveRechnung: CreateAndSaveRechnung,
  SendOneMail: _SendOneMail,
  sendRechnungen: sendRechnungen,
  compileRechnung: compileRechnung,
  rechnungenerstellen: rechnungenerstellen,
  createSepa: createSepa,
};
