﻿var express = require("express");
var router = express.Router();
var fs = require("fs");
var path = require("path");

global.members = require("./members.js");

//////////////////////////////////////////////////////////////////////////////////////////

function newsLetterFilename(year, number, published) {
  number = `0${number}`.slice(-2);

  let dir = __dirname + "/../Data/newsletter";
  dir = path.resolve(dir);


  var filename = `${dir}/${year}-${number}${published ? "-P" : ""}.json`;



  return filename;

}

function loadNewsletter(year, number) {

  let filename = newsLetterFilename(year, number, false);
  if (fs.existsSync(filename)) {
    var content = fs.readFileSync(filename);
    return JSON.parse(content);
  }

  filename = newsLetterFilename(year, number, true);
  if (fs.existsSync(filename)) {
    var content = fs.readFileSync(filename);
    return JSON.parse(content);
  }

  return undefined;
}

function saveNewsletter(year, number, published, data) {
  const dir = `Data/newsletter`;
  if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir);
  }


  var filename = newsLetterFilename(year, number, published);
  var content = JSON.stringify(data);
  fs.writeFileSync(filename, content);
}

//////////////////////////////////////////////////////////////////////////////////////////

router.post("/loadNewsletter", function (req, res) {
  try {
    const year = req.body.year;
    const number = req.body.number;

    res.send(loadNewsletter(year, number) || {});
  } catch (ex) { }
});

router.post("/loadLastNewsletter", function (req, res) {
  try {
    const names = getAllNewsletters();
    if (names.length > 0) {
      var filename = `Data/newsletter/${names[names.length - 1]}.json`;
      if (fs.existsSync(filename)) {
        res.send(content);
      }
    }
  } catch (ex) { }
  res.send("{}")
});


router.post("/saveNewsletter", function (req, res) {
  try {
    var year = req.body.year;
    var number = req.body.number;
    var published = req.body.published;

    saveNewsletter(year, number, published, req.body);

    res.send("@ok");
  } catch (ex) { }
});

///////////////////////////////////////////////////////////////////////////////////////////////

router.get("/allnewsletterImages", function (req, res) {
  try {
    let dir = __dirname + "/../Data/newsletterImage";
    dir = path.resolve(dir);

    const imagesName = [];

    var folder = _RechnungsFolder(RechnungsType);
    var files = fs.readdirSync(folder);
    for (var i in files) {
      var filename = files[i];
      imagesName.push(filename);
    }

    return { newsletterImages: imagesName };
  } catch (ex) {
    res.send("failed: " + ex.message);
  }
});

router.get("/newsletterImage", function (req, res) {
  try {
    let dir = __dirname + "/../Data/newsletterImage";
    dir = path.resolve(dir);

    const pictureName = req.query.pictureName;
    var filename = `${dir}/${pictureName}.jpg`;
    if (fs.existsSync(filename)) {
      res.sendFile(filename);
    } else {
      res.sendStatus(404);
    }
  } catch (ex) {
    res.send("failed: " + ex.message);
  }
});

router.post("/newsletterImage", function (req, res) {
  try {
    var data = req.body;

    var pictureName = data.pictureName;
    const img = data.img;

    const dir = `Data/newsletterImage`;
    if (!fs.existsSync(dir)) {
      fs.mkdirSync(dir);
    }

    var filename = `${dir}/${pictureName}.jpg`;

    var base64Data = img.replace(/^data:image\/png;base64,/, "");
    fs.writeFileSync(filename, base64Data, "base64");

    res.send("ok");
  } catch (ex) {
    res.send("failed: " + ex.message);
  }
});

/////////////////////////////////////////////////////////////////////////////////////////////

resultList = [];
sendNewletterDone = false;

router.get("sendStatus", function (req, res) {
  res.send({ sendNewletterDone: sendNewletterDone, resultList: resultList });
});

function _SendOneNewletter(member, toMail, html, newsletterName) {
  try {
    var nodemailer = require("nodemailer");

    var transporter = nodemailer.createTransport({
      host: "mail.incom.de",
      port: 25, // 465,
      secure: false, // true,
      auth: {
        user: "tcgw",
        pass: "Admin123!",
      },
      tls: {
        rejectUnauthorized: false,
      },
    });

    //   var transporter1und1 = nodemailer.createTransport(
    //     smtpTransport({
    //       service: "1und1",
    //       auth: {
    //         user: "noreply@tc-kreuzberg.de",
    //         pass: "tiebreak-mail",
    //       },
    //       tls: {
    //         rejectUnauthorized: false,
    //       },
    //     })
    //   );

    var fromMail = "Presse TC GW Am Kreuzberg e.V.<presse@tc-kreuzberg.de>";
    var subject = "TCGW Newsletter: " + newsletterName;
    var bcc = "";

    ////////////////////////////////////////////////////////////////
    //  TestBetrieb für den MailVersand hier ein und ausschalten !!!
    /////////////////////////////////////////////////////////////////
    var testBetrieb = true;
    /////////////////////////////////////////////////////////////////
    if (testBetrieb) {
      subject = "TEST: " + toMail + " " + newsletterName;
      toMail = "mail@michael-weller.de";
      //toMail = "info@diemachtderworte.de";
    }

    transporter.sendMail(
      {
        from: fromMail,
        to: toMail,
        bcc: bcc,
        subject: subject,
        html: html,
      },
      function (error, info) {
        try {
          if (error) {
            resultList.push({ MemberID: member.MemberID, Info: "ERROR: " + error });
          } else {
            resultList.push({ Info: toMail + " OK: Email gesendet" });
          }
        } catch (ex) {
          console.log("Error in _sendOneMail, transporter.sendMail, callback: " + ex.message);
        }
      }
    );
  } catch (ex) {
    console.log("Error in _SendOneNewletter: " + ex.message);
  }
}

router.post("/getSendStatus", function (req, res) {
  try {
    res.send({
      resultList: resultList,
      sendNewletterDone: sendNewletterDone
    })
  } catch (ex) {
    console.error(ex)
  }
})


router.post("/sendNewsletter", function (req, res) {
  try {
    var memberList = [...global.members.getMembers()];

    var year = req.body.year;
    var number = req.body.number;
    var html = req.body.html;

    const newsletterName = `TCGW Newsletter ${year}/${number}`;

    resultList = [];
    sendNewletterDone = false;

    const mx = 2;
    let cnt = 0;

    var currentMemberIndex = 0;


    var waiting = setInterval(function () {
      try {
        var member = memberList[currentMemberIndex++];

        while (!(member && member.Email && member.Aktiv != "false")) {
          var member = memberList[currentMemberIndex++];
          if (currentMemberIndex >= memberList.length) {
            clearInterval(waiting);
            return;
          }

        }

        if (member && member.Email && member.Aktiv != "false") {

          cnt++;
          if (cnt > mx) {
            clearInterval(waiting);
            return
          }

          if (currentMemberIndex < memberList.length) {
            if (member.Email) {
              let html1 = html.replaceAll("%%memberID", member.MemberID);
              html1 = html1.replaceAll("%%year", year);
              html1 = html1.replaceAll("%%number", number);

              _SendOneNewletter(member, member.Email, html1, newsletterName);
            } else {
              resultList.push({ Info: `Email addresse fehlt: ${member.FirstName} ${member.LastName}` });
            }
          } else {
            clearInterval(waiting);
          }
        }
      } catch (ex) {
        console.error(ex)
      }
    }, 2000);


    const filenameUnpublished = newsLetterFilename(year, number, false);
    const filenamePublished = newsLetterFilename(year, number, true);

    fs.renameSync(filenameUnpublished, filenamePublished);


    res.send("@running");
  } catch (ex) {
    console.log("Error: " + ex.message);
    res.send("Error: " + ex.message);
  }
});


function getAllNewsletters() {
  const names = [];

  try {
    let dir = __dirname + "/../Data/newsletter";
    dir = path.resolve(dir);

    var files = fs.readdirSync(dir);
    for (var i in files) {
      var filename = files[i];
      const ch = filename[0]
      if (ch >= "0" && ch <= "9") {
        names.push(filename.split(".")[0]);
      }
    }
  } catch (ex) { }

  return names;
}

router.post("/getAllNewsletters", function (req, res) {
  try {
    res.send({ names: getAllNewsletters() });
  } catch (ex) { }
});

router.get("/logo", function (req, res) {
  try {
    const year = req.query.y;
    let number = req.query.n;
    let source = req.query.s;
    const memberId = req.query.m;

    number = `0${number}`.slice(-2);

    console.log(`logo: ${year} / ${number} member=${memberId}`);

    let filename = __dirname + "/../Data/newsletter";
    filename += "/Kreuzberg-Logo-600x600.jpg";

    filename = path.resolve(filename);

    if (memberId != "@@M@@") {
      let filename2 = __dirname + "/../Data/newsletter";
      filename2 += `/readlog-${year}-${number}.txt`;
      filename2 = path.resolve(filename2);

      fs.appendFileSync(filename2, `${memberId}; ${source}; ${new Date().toLocaleString("de-DE", { timeZone: "Europe/Berlin" })} \n`);
    }

    res.sendFile(filename);
  } catch (ex) {
    console.error(ex);
    res.send("error: " + ex.message);
  }
});

router.post("/readlog", function (req, res) {
  try {
    const year = req.body.year;
    let number = req.body.number;

    number = `0${number}`.slice(-2);

    console.log(`readlog: ${year} / ${number}`);

    let filename = __dirname + "/../Data/newsletter";
    filename += `/readlog-${year}-${number}.txt`;

    filename = path.resolve(filename);

    if (fs.existsSync(filename)) {
      res.sendFile(filename);
    } else {
      res.send("")
    }
  } catch (ex) { }
});

module.exports = router;
