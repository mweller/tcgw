﻿var express = require("express");
var router = express.Router();
var moment = require("moment");
var moment_locale = require("moment/locale/de");

var members = require("./members.js");
var createSepa = members.createSepa;

moment.locale("de");

var fs = require("fs");
var request = require("request");
global.members = require("./members.js");

function formatIBAN(s) {
  return s.substr(0, 4) + " " + s.substr(4, 4) + " " + s.substr(8, 4) + " " + s.substr(12, 4) + " " + s.substr(16, 4) + " " + s.substr(20);
}

function isGuestBooking(booking) {
  for (var p in booking.partners) {
    var partner = booking.partners[p];
    if (partner.indexOf("GAST") >= 0) {
      return true;
    }
  }
  return false;
}

function loadGuestBookings(day) {
  var guestBookings = [];

  var bookings = loadBookings(day);
  for (var b in bookings) {
    var booking = bookings[b];
    if (isGuestBooking(booking)) {
      guestBookings.push(booking);
    }
  }

  return guestBookings;
}

function loadBookings(day) {
  var year = "2023";
  var filename = `public/Platzbelegung/${year}/${day}.json`;

  if (fs.existsSync(filename)) {
    var content = fs.readFileSync(filename);
    return JSON.parse(content);
  }
}

function filterGaeste(partners) {
  var filtered = [];
  for (var p in partners) {
    var partner = partners[p];
    var idx = partner.indexOf("GAST");
    if (idx >= 0) {
      var gast_partner = partner.substr(idx + 5).trim();
      if (gast_partner == "") {
        gast_partner = "Gast";
      }
      filtered.push(gast_partner);
    }
  }
  return filtered;
}

var EURO_PRO_STUNDE = 10;

var currentYear = new Date().getFullYear();
if (new Date().getMonth() < 11) {
  currentYear = new Date().getFullYear() - 1;
}

var RECHNUNG_GASTBUCHUNGEN = `Gastbuchungen ${currentYear}`;

function transformCSV(data) {
  var total = 0;

  var csvData = "";
  for (var i in data) {
    var member = data[i];
    var xMember = member.xmember;
    xMember["#" + RECHNUNG_GASTBUCHUNGEN] = 0;

    var gastDaten = [];
    csvData += i + ";" + member.gastBuchungen.length + ";" + member.gastBuchungen.length * EURO_PRO_STUNDE + "\n";
    total += member.gastBuchungen.length;
    for (var g in member.gastBuchungen) {
      var gastBuchungen = member.gastBuchungen[g];
      for (var j in gastBuchungen.gaeste) {
        var gast = gastBuchungen.gaeste[j];
        var doppel = "";
        if (gastBuchungen.doppel) {
          doppel = "Doppel";
        }
        csvData += ";;" + gast + ";" + gastBuchungen.date + ";" + doppel + "\n";
        gastDaten.push({ gast: gast, date: gastBuchungen.date, doppel: doppel });

        xMember["#" + RECHNUNG_GASTBUCHUNGEN] += EURO_PRO_STUNDE;
      }
    }

    xMember.gastDaten = gastDaten;
  }

  csvData += "total:;" + total + "\n";

  return csvData;
}

function transformData(bookings) {
  var xmembers = global.members.getMembers();

  var bookingsForMember = [];

  for (var b in bookings) {
    var booking = bookings[b];
    var id = booking["user[memberId]"];

    var name = booking["user[firstName]"] + " " + booking["user[lastName]"];

    if (bookingsForMember[name]) {
    } else {
      var member = xmembers[id];
      if (!member) {
        console.error("member not found " + id);
      }

      if (!member.AccountIBAN) {
        member = xmembers[member.MasterMemberID];
      }
      var data = {};

      if (member && member.AccountIBAN) {
        data = {
          iban: member.AccountIBAN,
          ibanF: formatIBAN(member.AccountIBAN),
          bic: member.AccountBIC,
          bankName: member.AccountBankName,
          kontoInhaber: member.AccountFirstName + " " + member.AccountLastName,
        };

        if (member.AccountLastName) {
          data.kontoInhaber = member.AccountFirstName + " " + member.AccountLastName;
        } else {
          data.kontoInhaber = member.FirstName + " " + member.LastName;
        }
      }
      bookingsForMember[name] = { member: data, gastBuchungen: [], xmember: member };
    }

    var doppel = booking.partners.length > 2;
    var gaeste = filterGaeste(booking.partners);
    var d = Date.parse(booking.date);
    var date = moment(d).locale("de").format("DD.MMMM HH:mm");
    bookingExtract = { date: date, court: booking.court, gaeste: gaeste, doppel: doppel };
    bookingsForMember[name].gastBuchungen.push(bookingExtract);
  }

  return bookingsForMember;
}

// .../rechnungGast/SEPA
router.get("/SEPA", function (req, res) {
  const RechnungsType = RECHNUNG_GASTBUCHUNGEN;

  createData();
  const { allErrors, doc } = createSepa(RechnungsType);

  if (allErrors != "") {
    res.send(allErrors);
    return;
  } else {
    var headers = {
      "Content-Type": "application/force-download",
      "Content-disposition": "attachment; filename=SEPA " + RechnungsType + moment(new Date()).format("-YYYY-MM-DD") + ".xml",
    };

    res.writeHead(200, headers);
    res.end(doc);
  }
});

// .../rechnungGast/rechnungenerstellen

router.get("/rechnungenerstellen", function (req, res) {
  createData();

  var url = req.protocol + "://" + req.headers.host + "/members/rechnungenerstellen?type=" + encodeURI(RECHNUNG_GASTBUCHUNGEN);
  res.redirect(url);
});

// .../rechnungGast/sendRechnungen
router.get("/sendRechnungen", function (req, res) {
  var url = req.protocol + "://" + req.headers.host + "/members/sendRechnungen?type=" + encodeURI(RECHNUNG_GASTBUCHUNGEN);
  res.redirect(url);
});

// test mwe: http://localhost:8089/rechnungGast/gastbuchungenForOneMember?id=1909

router.get("/gastbuchungenForOneMember", function (req, res) {
  var id = req.query.id;

  getAlleGastbuchungen(id).then((bookings) => {
    const bookingsForThisMember = [];

    for (var b in bookings) {
      const booking = bookings[b];
      if (booking["user[memberId]"] == id) {
        var doppel = booking.partners.length > 2;
        var gaeste = filterGaeste(booking.partners);
        var d = Date.parse(booking.date);
        var date = moment(d).locale("de").format("DD.MMMM HH:mm");
        bookingExtract = { date: date, court: booking.court, gaeste: gaeste, doppel: doppel };

        bookingsForThisMember.push(bookingExtract);
      }
    }

    var testString = "";
    for (var i = 0; i < bookingsForThisMember.length; i++) {
      const b = bookingsForThisMember[i];
      testString += `Datum: ${b.date} Platz: ${b.court} `;
      if (b.doppel) {
        testString += "Doppel: ";
      }

      testString += `Gast:`;
      for (var j = 0; j < b.gaeste.length; j++) {
        const g = b.gaeste[j];
        testString += g;
        if (j < b.gaeste.length - 1) {
          testString += " / ";
        }
      }
      testString += "<br/>";
    }

    const resData = {
      count: bookingsForThisMember.length,
      simpleList: testString,
      fullList: bookingsForThisMember,
    };
    res.send(resData);
  });
});

function getAlleGastbuchungen() {
  return new Promise(function (resolve, reject) {
    var resData = [];
    for (var day = 60; day < 330; day++) {
      var bookings = loadGuestBookings(day);
      if (bookings.length > 0) {
        resData.push(...bookings);
      }
    }

    resolve(resData);
  });
}

async function getNumberOfGastbuchungenForOneMember(memberId) {
  var allBookings = await getAlleGastbuchungen();
  for (var i in allBookings) {
    const booking = allBookings[i];
    console.log("boocking:", booking);
  }

  return 77;
}

function createData() {
  var resData = [];
  for (var day = 60; day < 330; day++) {
    var bookings = loadGuestBookings(day);
    if (bookings.length > 0) {
      resData.push(...bookings);
    }
  }

  var filename = "public/Platzbelegung/GastBuchungen.json";
  fs.writeFileSync(filename, JSON.stringify(resData, null, 2));

  var transformedData = transformData(resData);

  filename = "public/Platzbelegung/GastBuchungenTransformed.json";
  fs.writeFileSync(filename, JSON.stringify(transformedData, null, 2));

  var csvData = transformCSV(transformedData);
  filename = "public/Platzbelegung/GastBuchungen.csv";
  fs.writeFileSync(filename, csvData);
}

module.exports = router;
