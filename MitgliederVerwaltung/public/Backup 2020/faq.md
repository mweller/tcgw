# Wie oft kann ich im Voraus buchen ?
Du kannst immer nur **eine** Buchung haben. Diese kann bis zu 2 Tagen im voraus erfolgen.

Das ist so ähnlich, als ob Du Dein Schild an die Tafel hängst: Es kann immer nur einmal dort hängen

# Wann kann ich nach meinem Match erneut buchen?
Du kannst nach Deinem Match erneut buchen 

# Kann ich zusätzlich buchen, wenn jemand anderes für mich gebucht hat?
Nein. Es ist gleichgültig ob Du selbst buchst, oder jemand anderes Dich einträgt.
Du kannst immer nur eine Buchung haben   

# Doppel
Zum Buchen eines Doppels einfach 3 Partner eintragen.

Aufgrund der Systematik des Stundenrasters werden für Doppel automatsich **zwei Stunden** gebucht.
Hinter der angetippten Stunde muss somit eine weitere Stunde frei ist, 
sonst verweiget das System die Buchung

#Kann ich meine Buchung wieder löschen?
Ja Du kannst Deine Buchung jederzeit bis zum Beginn des Match löschen.
Dabei ist es unerheblich, ob Du selbst gebucht hast, oder jemand anderes Dich eingetragen hat.

Danach kann die Buchung nicht mehr gelöscht werden, und wird wegen Corona zur Kontaktdokumentation weiter gespeichert.


#Kann ich mit einem Gast spielen ?
Gastspieler sind uns willkommen und können als Partner mit dem Namen "Gast" gebucht werden.
Die Bezahlung und namentliche Erfassung des Gastes erfolgt bitte wie in der Vergangenheit bei Uwe. 

#Schnupperpass
Interessierte die gerne Mitglied des Vereins werden möchten, können einen Schnupperpass bekommen.
Im Rahmen der Ausstellung des Schnupperpasses werden Sie in der Mitgliederverwaltung als normale Vollmitglierder erfasst, und können dann ganz normal buchen.
Sollten sie dann nicht Mitglied werden wollen werden wir sie wieder löschen.


#Kann ich spantan zusätzlich spielen, wenn Plätze frei sind. Oder muss ich immer buchen?
Du musst immer buchen!!!

Allerdings ist es möglich spontan zusätzlich zu spielen, 
auch wenn Du bereits eine Verabredung hast.

Dazu gibt es die "Spontanbuchung"
* Ganz normal buchen
* Vor 14:00 Uhr maximal eine Stunde im Voraus 
* Im Club **jederzeit** für maximal 15 Minuten im Voraus.

#Welche Daten werden bei der Buchung von mir aufgezeichnet ?
Es werden zum einen die offensichtlichen Daten gespeichert:
* Dein Name 
* Der Deiner Partner/innen
* Uhr/Platz
* Wann die Buchung erfolgte

* Beim Löschen einer Buchung kann die Löschung gespeichert werden

Die Daten werden spätestens zum Ende des Saison gelöscht.
Wir behalten uns vor, abgelaufene Buchungen zu jeder Zeit zu löschen.

#Wer kann meine Buchung sehen ?
* Deine Buchung erscheint als "Reserviert" ohne Angaben der Namen in der Anzeige aller Mitglieder.
* Deine eigene Buchung ist für Dich und Deine Partner mit Namen lesbar.
* Auf der Anzeigetafel im Clubhaus sind alle Buchungen des Tages mit Namen lesbar.
* Auf einem "Clubhaus-/ Administrator-Bildschirm sind alle Buchungen der kommenden Tage lesbar.
* Alle Mitglieder, die mit dem WLAN des Clubhauses verbunden sind, können die Namen in der App wie auf der Anzeigetafel lesen

#Kann ich spontan im Clubbuchen wenn ich mein Handy vergessen habe ?
Das ist zwar grundsätzlich und technisch am "Clubhaus-Bildschirm" möglich. 
Wegen der Corona-beschränkungen zur Zeit aber leider noch nicht 



