# Wie oft kann ich im Voraus buchen ?

Du kannst immer nur **eine** Buchung haben. Diese kann bis zu 2 Tagen im voraus erfolgen.

Zusätzlich kannst Du jederzeit unbegrenzt spontan buchen.
Spontan heißt 2 Stunden im voraus bis 14 Uhr
Ansonsten 20 Minuten im voraus.

# Wann kann ich nach meinem Match erneut buchen?

Du kannst nach Deinem Match erneut buchen

# Kann ich zusätzlich buchen, wenn jemand anderes für mich gebucht hat?

Nein. Es ist gleichgültig ob Du selbst buchst, oder jemand anderes Dich einträgt.
Du kannst immer nur eine Buchung haben

# Doppel

Zum Buchen eines Doppels einfach 3 Partner eintragen.

Aufgrund der Systematik des Stundenrasters werden für Doppel automatsich **zwei Stunden** gebucht.
Hinter der angetippten Stunde muss somit eine weitere Stunde frei sein,
sonst verweiget das System die Buchung

#Kann ich meine Buchung wieder löschen?
Ja Du kannst Deine Buchung jederzeit bis zum Beginn des Match löschen.
Dabei ist es unerheblich, ob Du selbst gebucht hast, oder jemand anderes Dich eingetragen hat.

Danach kann die Buchung nicht mehr gelöscht werden.

#Kann ich mit einem Gast spielen ?
Gastspieler sind uns willkommen und können als Partner mit dem Namen "Gast" gebucht werden.
Du kannst bis zu 5 mal im Jahr einen Gast einladen.
Die Gastspielgebühr wir zu Ende der Saison vom Deinem Konto abgebucht

#Kann ich spontan zusätzlich spielen, wenn Plätze frei sind. Oder muss ich immer buchen?
Du musst immer buchen!!!

Allerdings ist es möglich spontan zusätzlich zu spielen,
auch wenn Du bereits eine Verabredung hast.

Dazu gibt es die "Spontanbuchung"

- Ganz normal buchen
- Vor 14:00 Uhr maximal eine Stunde im Voraus
- Im Club **jederzeit** für maximal 15 Minuten im Voraus.

#Welche Daten werden bei der Buchung von mir aufgezeichnet ?
Es werden zum einen die offensichtlichen Daten gespeichert:

- Dein Name
- Der Deiner Partner/innen
- Uhr/Platz
- Wann die Buchung erfolgte

- Beim Löschen einer Buchung kann die Löschung gespeichert werden

Die Daten werden spätestens zum Ende des Saison gelöscht.
Wir behalten uns vor, abgelaufene Buchungen zu jeder Zeit zu löschen.

#Wer kann meine Buchung sehen ?

- Deine eigene Buchung ist für Dich und Deine Partner mit Namen lesbar.

- Wenn Du Deine Buchungen auf "öffentlich" gestell hast, alle Mitglieder.
  Ansonsten erscheint Deine Buchung als "Reserviert" ohne Angaben Deines Namens

- Auf der Anzeigetafel im Clubhaus sind alle Buchungen des Tages mit Namen lesbar.

- Alle Mitglieder, die mit dem WLAN des Clubhauses verbunden sind, können die Namen in der App wie auf der Anzeigetafel lesen
