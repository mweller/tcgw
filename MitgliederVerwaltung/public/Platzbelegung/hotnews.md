# Außengastronomie ab Samstag, 22.Mai geöffnet - aktueller Test nötig!
Bitte haltet Euch Im Interesse von Uwe und des Vereins an unsere Hinweise auf der Terrasse 

# Corona / Bleibt gesund
Mit der Buchung eines Platzes übernimmt jedes Mitglied die Verantwortung zur Einhaltung der geltenden Regeln.
* Ab sofort sind Einzel und Doppel erlaubt
* Es ist jederzeit ein Abstand von 5m auf dem Platz einzuhalten. Außerhalb des Platzes 1,50m.
* Clubhaus, Umkleiden bleiben bis auf weiteres noch geschlossen.

# Verlassen der Anlage
Wir bitten die letzten Spieler am Tag, das Tor zur Anlage zu schließen. Vielen Dank.

#Kamera 
Die Kamera im Eingangsbereich ist nur in den Stunden außerhalb der Öffnungszeiten in Betrieb.

