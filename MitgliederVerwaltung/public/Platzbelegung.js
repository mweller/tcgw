//////////////////////////////////////////////////////////

Date.prototype.isLeapYear = function () {
  var year = this.getFullYear();
  if ((year & 3) != 0) return false;
  return ((year % 100) != 0 || (year % 400) == 0);
};

// Get Day of Year
Date.prototype.getDOY = function () {
  var dayCount = [0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334];
  var mn = this.getMonth();
  var dn = this.getDate();
  var dayOfYear = dayCount[mn] + dn;
  if (mn > 1 && this.isLeapYear()) dayOfYear++;
  return dayOfYear;
};

Date.prototype.addDays = function (days) {
  var date = new Date(this.valueOf());
  date.setDate(date.getDate() + days);
  return date;
}

Date.prototype.addHours = function (hours) {
  var date = new Date(this.valueOf());
  date.setHours(date.getHours() + hours);
  return date;
}

/////////////////////////////////////////////////////////////////////////////////

String.prototype.hashCode = function () {
  var hash = 0;
  if (this.length == 0) {
    return hash;
  }
  for (var i = 0; i < this.length; i++) {
    var char = this.charCodeAt(i);
    hash = ((hash << 5) - hash) + char;
    hash = hash & hash; // Convert to 32bit integer
  }
  return hash;
}

/////////////////////////////////////////////////////////////////////////////////

var initalBrowserZoomLevel = Math.round(window.devicePixelRatio * 100);





var ctx;
var darkMode = false

var rows = [];
var weekRows = [];

const days = ['Sonntag', 'Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag'];


var d = new Date();
var firstDay = new Date(d.getFullYear(), d.getMonth(), d.getDate());

var numOfDays = 3
var globalScale = 1

var topSpacingOnce = 50 * globalScale
var topSpacingPerDay = 100 * globalScale

var heightHour = 90 * globalScale;
var widthHour = 100 * globalScale;
var widthCourt = 150 * globalScale;
var scaleFactor = 1;
var firstHourOfDay = 6;
var lastHourOfDay = 23;
var offsetCourt456 = widthHour + widthCourt * 3;
var totalWidth = (widthHour * 4 + widthCourt * 7) / globalScale;

var heightOfOneDay = (lastHourOfDay - firstHourOfDay) * heightHour + 300 * globalScale;

var totalWidthUnscaled = totalWidth
var totalHeightUnscaled = heightOfOneDay * numOfDays

var fontMatch = (50 * globalScale) + "px Arial";
var fontMatchHome = (60 * globalScale) + "px Arial";
var fontTraining = (30 * globalScale) + "px Arial"
var fontHour = (40 * globalScale) + "px Arial";
var fontDate = (60 * globalScale) + "px Arial";

function getCourtStartTime(d) {
  var dNewTimes = new Date(2021, 6 - 1, 28);

  if (d instanceof Date) {
    console.log("d is date")
  } else {
    d = new Date(d)
  }

  if (d.getTime() < dNewTimes.getTime())
    return courtStartTime = [30, 30, 45, 45, 0, 0, 15]
  else
    return courtStartTime = [30, 30, 30, 0, 0, 0, 0]
}

// for (var c = 0; c < 7; c++) {
//   courtStartTime[c] = (c * 15) % 60;
// }

var ignoreTap = false

// Die Anzahl der Tage ab heute im Voraus, für die Buchung als Tag 0 startetn soll

var dayOffset = 0

//////////////////////////////////////////////////////////////////////
// reservierung

var maxDaysAheadAllowedForBooking = 2

var user = {}
var einzelReservierung = false
var bookingInfo = {}
var myNextBookingInfo = {}
var dayBookings = []
var memberlist = []
var partners = []

var openModalTimer = 0

var currentBookingIsMoving = false

var gastData = {}

//////////////////////////////////////////////////////////////////////

function updateSizes() {

  var c = document.getElementById("myCanvas");
  // globalScale = c.width / totalWidth / scaleFactor

  console.log("updateSizes globalScale=" + globalScale)


  topSpacingOnce = 50 * globalScale
  topSpacingPerDay = 100 * globalScale

  heightHour = 90 * globalScale;

  if (anzeigeTafelMode) {
    heightHour = 110 * globalScale;
  }


  widthHour = 100 * globalScale;
  widthCourt = 150 * globalScale;
  scaleFactor = 1;
  firstHourOfDay = 6;
  lastHourOfDay = 23;
  offsetCourt456 = widthHour + widthCourt * 3;

  heightOfOneDay = (lastHourOfDay - firstHourOfDay) * heightHour + 300 * globalScale;


  fontMatch = (50 * globalScale) + "px Arial";
  fontMatchHome = (60 * globalScale) + "px Arial";
  fontTraining = (30 * globalScale) + "px Arial"
  fontHour = (40 * globalScale) + "px Arial";
  fontDate = (60 * globalScale) + "px Arial";

}

function zfill(num, len) {return (Array(len).join("0") + num).slice(-len);}


function wrapText(context, text, x, y, maxWidth, lineHeight) {

  var words = text.split(" ");
  var line = "";

  for (var n = 0; n < words.length; n++) {

    var testLine = line + words[n] + " ";

    var metrics = context.measureText(testLine);

    var testWidth = metrics.width;

    if (testWidth > maxWidth) {

      context.fillText(line, x, y);

      line = words[n] + " ";
      y += lineHeight;
    } else {
      line = testLine;
    }

  }

  context.fillText(line, x, y);

}

function textLength(text) {
  var tl = ctx.measureText(text).width;
  return tl;
}

function fitFontSize(font, lines, width) {
  ctx.font = font;
  var f = 1

  for (var i in lines) {
    var text = lines[i]
    var f1 = width / (textLength(text) + 20);
    if (f1 < f) {
      f = f1
    }
  }

  var ss = font.split(' ')
  var fontSize = parseInt(ss[0]);

  if (f < 1) {
    fontSize *= f;
  }

  if (fontSize > 40) {
    fontSize = 40
  }
  ctx.font = fontSize + 'px ' + ss[1];

}


function drawOneRect(firstHour, numHours, firstCourt, numCourts, text, privateBooking, booking) {

  ctx.save();
  try {
    firstHour -= firstHourOfDay;

    firstCourt -= 1;



    while (firstCourt > 2) {
      ctx.translate(
        3 * widthCourt + widthHour,
        0);
      firstCourt -= 3;
    }

    ctx.translate(
      firstCourt * widthCourt + widthHour,
      firstHour * heightHour);

    // ctx.clearPath();

    var xIsBookingWithMe = false
    var showNames = false

    if (privateBooking) {
      xIsBookingWithMe = isBookingWithMe(booking)

      showNames = xIsBookingWithMe || kreuzbergMode

      var text = ""
      for (var p in booking.partners) {
        var partner = booking.partners[p]
        partnerName = partner.split(";")[1]
        if (partnerName) {
          var ss = partnerName.split(" ")

          if (partnerName.substr(0, 4) != "GAST") {
            ss[0] = ss[0].substr(0, 1) + "."
            partnerName = ss.join(" ")
          }

          //
          // falls die namen ohnehin angezeigt werden, 
          // oder der partner öffentlich bucht
          //
          var isPublic = partner.indexOf(";#pb") > 0
          if (showNames || isPublic) {
            text += partnerName + "\n"
          }


        } else {
          console.error("invalid partner: <" + partner + ">")
        }
      }
      text = text.substr(0, text.length - 1)

    }

    if (booking &&
      booking.partners && booking.partners.length > 0 &&
      parseInt(booking.partners[0].split(";")[0]) < 20) {
      privateBooking = false
      text = booking.partners[0].split(";")[1]
    }



    ctx.strokeStyle = "#ff0000";
    ctx.strokeStyle = 'black';
    ctx.fillStyle = 'black';
    ctx.textAlign = "center";
    ctx.font = fontTraining;


    var whiteText = false
    var isMark = (text == "@MARK")
    if (isMark) {
      console.log(`drawOneRect: court ${firstCourt} y=${firstHour * heightHour} firstHour=${firstHour} heightHour=${heightHour}`)
      var sMinutes = bookingInfo.minutes
      if (sMinutes == "0") {
        sMinutes = "00";
      }
      text = bookingInfo.hour + ":" + sMinutes
      ctx.fillStyle = "#00a";
      whiteText = true;
    } else if (privateBooking) {
      if (xIsBookingWithMe) {
        ctx.fillStyle = "blue";
        if (currentBookingIsMoving && myNextBookingInfo.uid == booking.uid) {
          text = "VERSCHIEBEN"
        }
        whiteText = true;

      } else {
        ctx.fillStyle = "yellow";
        whiteText = false;
      }
    } else {
      ctx.fillStyle = "orange";
    }
    ctx.opacity = 1;



    var dd = 2;
    ctx.fillRect(dd, 0,
      numCourts * widthCourt - dd,
      numHours * heightHour - dd
    );


    if (whiteText) {
      ctx.fillStyle = 'white';
    } else {
      ctx.fillStyle = 'black';
    }



    var lines = text.split("\n");

    var dy = 0

    var lineSpacing = (heightHour * numHours) / (lines.length + 1)

    if (lines.length > 1) {
      dy = lineSpacing * lines.length / 2
    }

    if (isMark) {
      dy = 0;
    }

    fitFontSize(fontTraining, lines, numCourts * widthCourt)

    ctx.translate(0, -dy);

    for (var i in lines) {
      var line = lines[i]

      ctx.fillText(
        line,
        numCourts * widthCourt / 2,
        numHours * heightHour / 2 + 15);

      ctx.translate(0, lineSpacing);

    }


  } catch (ex) {
    console.error(ex)
  }
  ctx.restore();

}

function drawOneRectMedenspiel(firstHour, numHours, firstCourt, numCourts, text) {

  var orgNumCourts = numCourts;
  var widthGab = 0

  if (Math.floor((firstCourt - 1) / 3) != Math.floor(((firstCourt - 1) + numCourts - 1) / 3)) {
    widthGab = widthHour
  }

  firstHour -= firstHourOfDay;
  firstCourt -= 1;


  ctx.save();

  console.log(firstCourt);
  if (firstCourt > 2) {
    ctx.translate(
      3 * widthCourt + widthHour,
      0);
    firstCourt -= 3;
  }

  ctx.translate(
    firstCourt * widthCourt + widthHour,
    firstHour * heightHour);


  ctx.strokeStyle = "#ff0000";
  ctx.fillStyle = "#90ee90f0";
  ctx.opacity = 1;

  var dd = 2;
  ctx.fillRect(dd, dd,
    numCourts * widthCourt - 2 * dd + widthGab,
    numHours * heightHour - 2 * dd
  );


  ctx.strokeStyle = 'black';
  ctx.fillStyle = 'black';
  ctx.textAlign = "center";
  ctx.font = fontMatch;


  var vv = 60 * globalScale;

  var ss = text.split(' gegen ');


  fitFontSize(fontMatch, ss, numCourts * widthCourt);

  ctx.fillText(
    ss[1],
    numCourts * widthCourt / 2 + widthGab,
    numHours * heightHour / 2 + vv);

  ctx.fillText(
    '-',
    numCourts * widthCourt / 2 + widthGab,
    numHours * heightHour / 2);

  ctx.fillText(
    ss[0],
    numCourts * widthCourt / 2 + widthGab,
    numHours * heightHour / 2 - vv);

  ctx.restore();

}

function getRowsForDay(day, weekDay) {
  var res = [];

  for (var i in rows) {
    var row = rows[i];
    // console.log("getRowsForDay " + day + "  " + row.day);
    if (row.day == day) {
      // console.log("push row");
      res.push(row);

    }
  }

  for (i in weekRows) {
    row = weekRows[i];

    if (row.day == weekDay.toUpperCase()) {
      // console.log("push row");
      res.push(row);

    }
  }

  return res;
}

function isBookingWithMe(booking) {
  if (clubhausMode) {
    return false
  }

  for (var p in booking.partners) {
    var partner = booking.partners[p]
    if (partner.indexOf(user.memberId) >= 0) {
      return true
    }
  }

  return false
}

function isBookingInThePast(booking) {
  var d = new Date(booking.date)
  d = d.addHours(1)
  return d < new Date()
}



function drawOneDay(dayIndex, date, day, weekDay, thisDayBookings) {

  try {
    switch (dayIndex + dayOffset) {
      case 0:
        weekDay = "Heute " + weekDay;
        break;

      case 1:
        weekDay = "Morgen " + weekDay;
        break;

      case 2:
        weekDay = "Übermorgen " + weekDay;
        break;

    }

    drawDate(day, weekDay);

    ctx.save();
    ctx.translate(0, topSpacingPerDay);

    ctx.save();
    drawBackgroud3(1, date, day);
    ctx.restore();


    ctx.save();
    ctx.translate(offsetCourt456, 0);
    drawBackgroud3(4, date, day);
    ctx.restore();

    ctx.save();
    ctx.translate(offsetCourt456 * 2, 0);
    drawBackgroud3(7, date, day);
    ctx.restore();

    var firstHour = 0;
    var numHours = 3;
    var firstCourt = 0;
    var numCourts = 3;
    var text = 'tc Kreuzberg vs. gegener';


    if (bookingInfo.dayIndex == dayIndex) {
      drawOneRect(bookingInfo.hour + bookingInfo.minutes / 60, 1, bookingInfo.court, 1, "@MARK", false)
    }


    //drawOneRect (firstHour, numHours, firstCourt, numCourts, text);

    //drawOneRect (firstHour, numHours, firstCourt, numCourts, text);


    for (var b in thisDayBookings) {
      var booking = thisDayBookings[b]

      var numHours = 1
      if (booking.numHours) {
        numHours = parseInt(booking.numHours)
      }

      var court = parseInt(booking.court)
      var firstHour = parseInt(booking.hour) + (parseInt(booking.minutes) / 60)

      if (booking.type == 'Training') {
        drawOneRect(firstHour, numHours, court, booking.numCourts, booking.text);
      } else if (booking.type == 'Medenspiel') {
        drawOneRectMedenspiel(firstHour, numHours, court, booking.numCourts, booking.text);
      } else {
        drawOneRect(firstHour, numHours, court, 1, text, true, booking);
      }
    }

    if (dayIndex + dayOffset <= 0) {
      drawPastTime()
    }


    ctx.restore();
  } catch (ex) {
    console.error(ex.message)
  }
}

function drawOneWeek() {
  ctx.save();


  d = new Date(firstDay)
  var dayIndex = d.getDOY()

  if (anzeigeTafelMode) {
    numOfDays = 1
  }

  for (var i = 0; i < numOfDays; i++) {
    // console.log(d);

    var iWeekday = d.getDay();
    var day = zfill(d.getDate(), 2) + '.' + zfill(d.getMonth() + 1, 2) + '.' + d.getFullYear();

    drawOneDay(i, d, day, days[iWeekday], dayBookings[i + dayIndex]);
    ctx.translate(0, heightOfOneDay);
    d.setDate(d.getDate() + 1);
  }
  ctx.restore();
}


function drawDate(day, weekDay) {
  if (darkMode) {
    ctx.fillStyle = "white";
  } else {
    ctx.fillStyle = "#000000";
  }

  ctx.font = fontDate;
  ctx.textAlign = "start";

  ctx.fillText(weekDay + '   ' + day, widthHour, 25);
}


function drawTimes() {

  if (darkMode) {
    ctx.fillStyle = "white";
  } else {
    ctx.fillStyle = "#000000";
  }

  ctx.font = fontHour;
  // ctx.textAlign = "end";

  var y = heightHour;
  for (var h = firstHourOfDay + 1; h <= lastHourOfDay - 1; h++) {
    ctx.fillText(h, widthHour * 0.3, y + 20);
    y += heightHour;
  }
}

function drawCourts(firstCourt) {

  if (darkMode) {
    ctx.fillStyle = "white";
  } else {
    ctx.fillStyle = "#000000";
  }

  ctx.font = fontHour;
  ctx.textAlign = "end";

  var y = 0;
  var x = widthHour + widthCourt / 2;

  var mx = 3;
  if (firstCourt == 7) {
    mx = 1
  }

  for (var h = 0; h < mx; h++) {
    ctx.fillText(h + firstCourt, x, y - 20);
    x += widthCourt;
  }

}


function drawPastTime() {

  var d = new Date()
  var dd1 = d.getHours() + d.getMinutes() / 60
  var h = (dd1 - firstHourOfDay) * heightHour;

  if (dayOffset < 0) {
    h = heightHour * (lastHourOfDay - firstHourOfDay)
  } else {
    if (h < 0 || h > heightHour * (lastHourOfDay - firstHourOfDay)) {
      return;
    }

  }

  ctx.fillStyle = "#66aa66a0";
  ctx.strokeStyle = 'transparent';
  ctx.lineWidth = 0;

  ctx.fillRect(0, -15, totalWidth, h + 15);

  ctx.fillStyle = "brown";
  ctx.strokeStyle = 'black';
  ctx.lineWidth = 50;

  ctx.fillRect(0, h - 8, totalWidth, 8);

}


function drawBackgroud3(firstCourt, date, day) {

  var mx = 3;
  if (firstCourt == 7) {
    mx = 1
  }

  var h = (lastHourOfDay - firstHourOfDay) * heightHour;

  ctx.fillStyle = "#00bb00";
  ctx.strokeStyle = 'black';
  ctx.lineWidth = 10;

  ctx.fillRect(widthHour,
    0,
    (mx) * widthCourt,
    h);

  if (darkMode) {
    ctx.fillStyle = "white";
  } else {
    ctx.fillStyle = "#000000";
  }


  for (var i = 0; i <= mx; i++) {
    ctx.fillRect(
      i * widthCourt + widthHour - 0.5,
      0,
      2,
      h);
  }



  var courtStartTime = getCourtStartTime(date)

  for (var court = 0; court < mx; court++) {

    const yOffset = courtStartTime[court + firstCourt - 1] / 60 * heightHour;

    for (i = 0; i < (lastHourOfDay - firstHourOfDay); i++) {
      ctx.fillRect(
        widthHour + court * widthCourt,
        i * heightHour - 0.5 + yOffset - 3,
        widthCourt,
        6);
    }
  }

  ctx.save()
  drawCourts(firstCourt);
  ctx.restore()


  drawTimes();

}


function loadDays(callback) {
  $.get("Platzbelegung/Medenspiele.csv"
    , function (data) {
      $(".result").html(data);

      lines = data.split('\n');

      for (var i in lines) {
        var row = lines[i].split(";");
        try {
          dRow = {
            raw: row,
            type: 'Medenspiel',
            day: row[0].trim(),
            startTime: row[1].trim(),
            endTime: row[2],
            courts: row[3],
            match: row[4]
          };
          rows.push(dRow);
        } catch (ex) {}

        // console.log(row);
        // console.log(row[0]);
      }
      if (callback) {
        callback();
      }

    });
}

function loadBookings(callback) {
  var d = new Date()
  d = d.addDays(dayOffset)

  $.post("booking/getbookings",
    {
      date: d,
      anzeigeTafelMode: anzeigeTafelMode
    },
    function (data) {
      dayBookings = JSON.parse(data)
      if (callback) {
        callback();
      }

    });
}

function findClickedCourtAndTime(x, y) {

  var yScaled = y / scaleFactor
  var xScaled = x / scaleFactor

  xScaled -= widthHour

  var court = 1
  if (xScaled > widthCourt * 3) {
    xScaled -= widthHour + widthCourt * 3
    var court = 4
  }

  if (xScaled > widthCourt * 3) {
    xScaled -= widthHour + widthCourt * 3
    var court = 7

  }

  yScaled -= topSpacingOnce

  court += Math.floor(xScaled / widthCourt);
  var foundDayIndex = Math.floor(yScaled / heightOfOneDay)


  yScaled -= heightOfOneDay * foundDayIndex
  yScaled -= topSpacingPerDay

  var hoursPerDay = (lastHourOfDay - firstHourOfDay) + 1
  var h = hoursPerDay * heightHour

  var time = yScaled / h * hoursPerDay

  time += firstHourOfDay

  var minuteStep = 15
  var hour = Math.floor(time)
  var minutes = (time - hour) * 60
  minutes = Math.round(minutes / minuteStep) * minuteStep
  if (minutes == 60) {
    minutes = 0
    hour += 1
  }


  // console.log(`court ${court} day=${foundDayIndex} time=${time}`)

  return {
    hour: hour,
    minutes: minutes,
    time: time,
    court: court,
    dayIndex: foundDayIndex
  }

}

function findMyCurrentBooking() {

  if (clubhausMode) {
    return
  }

  bookingInfo = {}
  myNextBookingInfo = {}

  var found = false

  for (var d in dayBookings) {
    var oneDayBooking = dayBookings[d]
    for (var b in oneDayBooking) {
      var booking = oneDayBooking[b]
      if (isBookingWithMe(booking) &&
        !isBookingInThePast(booking)) {

        if (myNextBookingInfo.date) {
          if (new Date(myNextBookingInfo.date) > new Date(booking.date)) {
            myNextBookingInfo = booking
            found = true
          }
        } else {
          myNextBookingInfo = booking
          found = true
        }
      }
    }
  }



  return found;

}

function resizeCanvas() {

  // $("#myCanvas").attr("width", totalWidthUnscaled);
  // $("#myCanvas").attr("height", totalHeightUnscaled);

  // $("#myCanvas").attr("width", 5000);
  // $("#myCanvas").attr("height", 5000);

}


function drawAll() {

  resizeCanvas()
  var c = document.getElementById("myCanvas");
  ctx = c.getContext("2d");

  debugOutput("drawAll: scaleFactor=" + scaleFactor)
  updateSizes();


  //
  // Fill background
  //
  if (!darkMode) {
    ctx.fillStyle = "white";
  } else {
    ctx.fillStyle = "#000000";
  }
  ctx.fillRect(0, 0, c.width, c.height);

  ctx.fillRect(0, 0, 5000, 5000);

  ctx.save();
  ctx.scale(scaleFactor, scaleFactor);

  ctx.translate(0, topSpacingOnce);

  drawOneWeek();


  debugOutput("drawAll: done")

  ctx.restore();

}

function checkValidPin() {
  user = JSON.parse(localStorage.getItem('user'))
  id = localStorage.getItem('id');
  var pin = localStorage.getItem('pin');

  if (clubhausMode) {
    id = "97531"
  }


  if (anzeigeTafelMode) {

  } else {
    $.get(`booking/checkvalid?id=${id}&pin=${pin}`, function (data) {
      if (data != "@ok") {
        window.location.href = `/booking/register?id=${id}&pin=${pin}&error=idpin`;
      }
    })
  }

}

function findBookingForClick(clickInfo) {

  alignStarttime(clickInfo)

  d = new Date(firstDay)
  var dayIndexToday = d.getDOY()


  var oneDayBooking = dayBookings[dayIndexToday + clickInfo.dayIndex]
  for (var b in oneDayBooking) {
    var booking = oneDayBooking[b]

    var numHours = 1

    if (booking.numHours) {
      numHours = parseInt(booking.numHours)
    } else {
      console.log("no numHours")
    }


    if (parseInt(booking.court) == clickInfo.court &&
      (parseInt(booking.hour) == clickInfo.hour ||
        ((parseInt(booking.hour) + numHours - 1) == clickInfo.hour))) {

      myNextBookingInfo = booking
      showBooking($("#deineBuchung"), myNextBookingInfo)
      $(".deineBuchungTop").show()
      $("#htmlHotNews").hide()

      // $([document.documentElement, document.body]).animate({
      //   scrollTop: $("#deineBuchung").offset().top
      // }, 500);

      return true
    }
  }

  return false;
}

function showPublicPrivateBooking() {

  try {
    $.get(`/booking/getpublicbooking?id=${user.memberId}`, function (data) {
      if (data == "public") {
        $("#makeBookingPrivate").show()
        $("#makeBookingPublic").hide()
      } else {
        $("#makeBookingPrivate").hide()
        $("#makeBookingPublic").show()
      }
    });
  } catch (ex) {

  }
}

$("#3daysForward").click(function () {
  dayOffset += 3
  var d = new Date()
  firstDay = new Date(d.getFullYear(), d.getMonth(), d.getDate());
  firstDay = firstDay.addDays(dayOffset)
  reloadBookingsAndDrawAll();

});

$("#3daysBackward").click(function () {
  dayOffset -= 3
  var d = new Date()
  firstDay = new Date(d.getFullYear(), d.getMonth(), d.getDate());
  firstDay = firstDay.addDays(dayOffset)
  reloadBookingsAndDrawAll();

});

(function () {
  // page startup
  // initialization
  //

  if (anzeigeTafelMode) {
    setInterval(function () {
      var d = new Date()
      firstDay = new Date(d.getFullYear(), d.getMonth(), d.getDate());
      reloadBookingsAndDrawAll();
    }, 60 * 1000)
  }

  if (!anzeigeTafelMode && !clubhausMode && !kreuzbergMode) {
    $.get("/booking/checkKreuzbergMode", function (data) {
      if (data == "@ok") {
        kreuzbergMode = true;
        $("#user").html("Hallo " + user.firstName + ",<br/> willkommen in Club")
        drawAll();
      }
    })
  }

  console.log("start");
  debugOutput("start")

  // 
  // autohide hotNews if Unchanged
  //
  var hashCodeCurrent = $("#htmlHotNews").text().hashCode()
  var hashCodeLast = localStorage.getItem('hotNewsHash');
  if (hashCodeCurrent == hashCodeLast) {
    $("#htmlHotNews").hide()
  }


  $("#gastModal").hide()

  $("#moveCurrentBooking").hide()

  user = JSON.parse(localStorage.getItem('user'))

  checkValidPin()

  showPublicPrivateBooking()

  $("#makeBookingPublic").click(function () {
    $.get(`/booking/setpublicbooking?id=${user.memberId}&value=true`, function (data) {
      showPublicPrivateBooking()
    });
  });

  $("#makeBookingPrivate").click(function () {
    $.get(`/booking/setpublicbooking?id=${user.memberId}&value=false`, function (data) {
      showPublicPrivateBooking()
    });
  });



  var color = $("#top").css("background-color");
  darkMode = color.indexOf("255") < 0

  debugOutput("color=" + color + " darkMode=" + darkMode)


  if (anzeigeTafelMode) {
    user = "anzeigeTafel"
  } else {
    if (user) {
      $("#user").html("Hallo " + user.firstName)
    } else {
      window.location.href = "/booking/register";
      return
    }
  }


  $(".deineBuchungTop").hide()

  loadBookings(function () {

    debugOutput("call drawAll")

    drawAll();

    if (findMyCurrentBooking()) {
      showBooking($("#deineBuchung"), myNextBookingInfo)
      $(".deineBuchungTop").show()
      $("#htmlHotNews").hide()
    } else {
      $(".deineBuchungTop").hide()
    }
  });

  //
  // center modal dialog
  //

  var m = $("#bookingModal");
  var xleft = ($(window).width() - m.width()) / 2
  m.offset({top: 0, left: xleft});

  m = $("#gastModal");
  xleft = ($(window).width() - m.width()) / 2
  m.offset({top: 0, left: xleft});

  $('#closeCurrentBooking').click(function (ev) {
    $('#deineBuchung').hide()
  })

  $('#myCanvas').on("taphold", function (event) {
    ev.preventDefault()
    // alert("taphold")
  })

  $('#myCanvas').mousedown(function (ev) {
    ev.preventDefault()
  });


  $('#myCanvas').click(function (ev) {


    if (ignoreTap) {
      ignoreTap = false;
      return
    }

    var f = totalWidthUnscaled / $('#myCanvas').width()

    // f = 1 // globalScale
    var clickInfo = findClickedCourtAndTime(ev.offsetX * f, ev.offsetY * f)

    if (clickInfo.hour > lastHourOfDay - 1) {
      return;
    }

    var d = new Date()
    var dd1 = d.getHours() + d.getMinutes() / 60
    var dd2 = clickInfo.dayIndex * 24 + clickInfo.hour + clickInfo.minutes / 60


    var dHours = dd2 - dd1;
    if ((dHours < -0.15 && (clickInfo.dayIndex + dayOffset == 0)) ||
      (clickInfo.dayIndex + dayOffset < 0)) {

      myAlert("Du kannst nicht in der Vergangenheit reservieren", "Das wolltest Du nicht ....")
    } else {

      console.log(`${ev.offsetX} ${ev.offsetY} ${f} ->  ${ev.offsetX * f} ${ev.offsetY * f}`)

      if (clubhausMode && !currentBookingIsMoving) {
        if (findBookingForClick(clickInfo))
          return;
      }

      if (currentBookingIsMoving) {

        bookingInfo = JSON.parse(JSON.stringify(myNextBookingInfo))

        bookingInfo.dateModified = new Date()
        bookingInfo.userModified = user
        bookingInfo.court = clickInfo.court
        bookingInfo.hour = clickInfo.hour
        bookingInfo.minutes = clickInfo.minutes
        bookingInfo.dayIndex = clickInfo.dayIndex
        alignStarttime(bookingInfo)

        $.post(
          'booking/moveBooking', bookingInfo,
          function (data) {
            if (data != "@ok") { // check error
              myAlert(data, "Fehler beim Verschieben")
            } else {
              currentBookingIsMoving = false
              $("#moveCurrentBooking").hide()
              reloadBookingsAndDrawAll()
            }
          }
        );

      } else {
        openReservation(clickInfo)
        // alert(`court ${clickInfo.court} day=${clickInfo.dayIndex} time=${clickInfo.time}`)
      }
    }


  });


})();



function alignStarttime(bookingInfo) {
  // mwe1

  var d = new Date(bookingInfo.date)

  var courtStartTime = getCourtStartTime(d)
  var startMinutes = courtStartTime[bookingInfo.court - 1]

  if (bookingInfo.minutes < startMinutes) {
    bookingInfo.hour -= 1
  }

  bookingInfo.minutes = startMinutes
  d.setHours(bookingInfo.hour, bookingInfo.minutes)
  bookingInfo.date = d

}

function checkBookingAllowed(bookingInfo, callbackError, callbackOk) {

  bookingInfo.dateCreated = new Date()

  $.post("booking/checkBookingAllowed", bookingInfo, function (data) {
    if (data.substr(0, 3) != "@ok") {
      callbackError(data)
    } else {
      callbackOk(data)
    }
  });
}

function showBooking(root, bookingInfo) {
  var sMinutes = bookingInfo.minutes
  if (sMinutes == 0) {
    sMinutes = "00"
  }

  var d = new Date(bookingInfo.date)
  d = new Date(d.getFullYear(), d.getMonth(), d.getDate())
  d.setHours(parseInt(bookingInfo.hour), parseInt(bookingInfo.minutes), 0, 0)

  root.find(".court").text(bookingInfo.court)
  root.find(".time").text(bookingInfo.hour + ":" + sMinutes)
  root.find(".date").text(days[d.getDay()] + "  " + d.getDate() + "." + (d.getMonth() + 1) + "." + d.getFullYear())

  partners = []
  root.find(".partnerSelection").show()

  if (bookingInfo.type == "Medenspiel") {
    root.find(".deinePartner").html("Medenspiel<br/>" + bookingInfo.match)
    root.find(".court").text(bookingInfo.courts)
  } else {
    if (!clubhausMode) {
      if (!einzelReservierung) {
        root.find(".deinePartner").html("Dein(e) Partner(innen)")
      } else {
        root.find(".deinePartner").html("Dein(e) Partner(in)")
      }
    }

    showPartners(root, bookingInfo.partners)
  }

  //
  // Enable/Disable Verschieben und Löschen 
  // 

  var xnow = new Date()

  if (d < xnow) {
    $("#delete").prop('disabled', true);
    $("#move").prop('disabled', true);
  } else {
    $("#delete").prop('disabled', false);
    $("#move").prop('disabled', false);
  }

  if (bookingInfo.uidSerie) {
    $("#deleteSerie").show()
  } else {
    $("#deleteSerie").hide()
  }

  $("#htmlHotNews").hide()


}

function unZoom() {
  let restore = $('meta[name=viewport]')[0];


  $('meta[name=viewport]').remove();
  $('head').append('<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">');
  if (restore) {
    setTimeout(() => {
      $('meta[name=viewport]').remove();
      $('head').append(restore);
    }, 100); // On Firefox it needs a delay > 0 to work
  }
}

function openReservation(clickInfo) {

  $("#reservierenSerie").hide()

  if (memberlist.length == 0) {
    $.get("booking/memberlist", function (data) {
      memberlist = data.split("\n");
      if (clubhausMode) {
        memberlist.push("0001;Training")
        memberlist.push("0002;Training Jugend")
        memberlist.push("0003;Training Mannschaft")
        memberlist.push("0004;Training Privat")
        memberlist.push("0005;Veranstaltung")
        memberlist.push("0006;Wettkampf")
        memberlist.push("0007;Medenspiel")
        memberlist.push("0008;Pokal")
        memberlist.push("0009;GESPERRT-2")
        memberlist.push("0010;GESPERRT-6")

      }

      memberlist.push("9999;GAST")
    });
  }



  if (!clubhausMode) {
    if (clickInfo.dayIndex > maxDaysAheadAllowedForBooking) {
      myAlert("heute oder die kommenden ${maxDaysAheadAllowedForBooking} Tage", "Ein Platzreservierung ist nur möglich für");
      return;
    }
  }

  var d = new Date()
  var d = new Date(d.getFullYear(), d.getMonth(), d.getDate() + clickInfo.dayIndex, clickInfo.hour, clickInfo.minutes);
  d = d.addDays(dayOffset)

  partners = []
  bookingInfo = {
    court: clickInfo.court,
    date: d,
    hour: clickInfo.hour,
    minutes: clickInfo.minutes,
    dayIndex: clickInfo.dayIndex,
    partners: [],
    user: user
  }

  if (!clubhausMode) {
    bookingInfo.partners[partners.length] = (user.memberId + ";" + user.firstName + " " + user.lastName)
  }

  alignStarttime(bookingInfo)

  checkBookingAllowed(bookingInfo,
    function (errorMessage) {
      myAlert(errorMessage, "Reservierung nicht möglich")
      // bookingInfo = {}
      drawAll()
    },
    function (data) {


      if (data == "@ok spontan") {
        $("#bookingModal h1").html("Spontan-Reservierung vornehmen")
      } else {
        $("#bookingModal h1").html("Reservierung vornehmen")
      }

      drawAll();


      bookingInfo.partners = [];

      showBooking($("#bookingModal"), bookingInfo);



      $("#filerNames").val("")
      $("#names").html("")
      $(".partners").html("")
      $("#test_area").hide()

      $("body").css("cursor", "wait");

      var browserZoomLevel = Math.round(window.devicePixelRatio * 100);
      currentZoomLevel = browserZoomLevel / initalBrowserZoomLevel

      clearTimeout(openModalTimer)

      $("#reservieren").attr("disabled", true)

      openModalTimer = setTimeout(function () {
        unZoom();

        $('#deineBuchung').hide()

        gastData = {}
        var m = $("#bookingModal");
        m.modal({keyboard: true})
        setTimeout(function () {
          $("body").css("cursor", "default");
          $("#filerNames").focus()
        }, 500)
      }, 500);

    });

}


function reloadBookingsAndDrawAll() {

  loadBookings(function () {
    if (findMyCurrentBooking()) {
      showBooking($("#deineBuchung"), myNextBookingInfo)
      $(".deineBuchungTop").show()
      $("#htmlHotNews").hide()
    } else {
      $(".deineBuchungTop").hide()
    }

    drawAll();
  });

}

function reservierenDone(data) {

  if (data == "@ok") {
    $("#test_area").css("background-color", "orange")
    $("#msg1").html("Reservierung ist erfolgt")


    bookingInfo = {}
    reloadBookingsAndDrawAll();

    var m = $("#bookingModal");
    var id = setTimeout(function () {
      m.modal('hide');
    }, 1000);


  } else {
    $("#test_area").css("background-color", "red")
    $("#msg1").html("Reservierung konnte nicht durchgeführt werden: <br/>" + data)

  }
}


function deleteCurrentBooking() {
  myConfirm("Willst Du die Buchung wirklich löschen",
    function () {
      $.post(
        'booking/delete', myNextBookingInfo,
        function (data) {
          reloadBookingsAndDrawAll();
        }
      );
    });

}

function deleteCurrentBookingSerie() {
  myConfirm("Willst Du die Serien-Buchung wirklich löschen",
    function () {
      $.post(
        'booking/deleteSerie', myNextBookingInfo,
        function (data) {
          reloadBookingsAndDrawAll();
        }
      );
    });

}

function moveCurrentBooking() {
  myConfirm("Zum Verschieben der Buchung tippe auf eine anderen freien Platz/Uhrzeit",
    function () {
      currentBookingIsMoving = true
      $("#moveCurrentBooking").show()
      drawAll()
    });

}


var gastModalCloseWithOk = false

function bestaetigenGastDaten() {
  var iban = $("#GastIBAN").val()
  var inhaber = $("#GastKontoInhaber").val()
  var gastName = $("#gastName").val()
  var gastIstJugendlich = $("#GastIstJugendlich")[0].checked

  gastData = {
    iban: iban,
    name: gastName,
    KontoInhaber: inhaber,
    gastIstJugendlich: gastIstJugendlich
  }

  var i = partners.length - 1
  partners[i] = partners[i] + " " + gastName

  showPartners($("#bookingModal"), partners);

  gastModalCloseWithOk = true;
  $("#gastModal").modal('hide');

}

$('#gastModal').on('hidden.bs.modal', function (e) {
  if (!gastModalCloseWithOk) {
    partners = partners.slice(0, -1)
    showPartners($("#bookingModal"), partners);
    updateAfterMemberClickedAndChecked();
  }
})


$('#bookingModal').on('hidden.bs.modal', function (e) {
  bookingInfo = {};
  drawAll();
})


function uuidv4() {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
    var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
    return v.toString(16);
  });
}
function reservierenSerie() {
  bookingInfo.uidSerie = uuidv4()
  reservieren()
}

function reservieren() {

  $("#test_area").css("background-color", "yellow")
  $("#msg1").html("Reservierung wird gesendet ..")
  $("#test_area").show()

  bookingInfo.partners = partners
  if (!clubhausMode) {
    bookingInfo.partners[partners.length] = (user.memberId + ";" + user.firstName + " " + user.lastName)
  }

  bookingInfo.dateCreated = new Date()
  bookingInfo.gastData = gastData

  $.post(
    'booking/book', bookingInfo,

    function (data) {
      reservierenDone(data);
    }
  );



}


$(".modal-wide").on("show.bs.modal", function () {
  var height = $(window).height() - 200;
  $(this).find(".modal-body").css("max-height", height);
});


//
// prüfen ob member bereits in der buchung enthalten ist  
//
function memberIsPartOfBooking(potentialPartner) {

  if (!clubhausMode) {
    if (potentialPartner.indexOf(user.memberId) >= 0) {
      // ich bin es selbst
      return true
    }
  }

  for (p in partners) {
    var partner = partners[p]
    if (partner == potentialPartner) {
      return true
    }
  }

  return false
}

$("#filerNames").keyup(function () {
  var filter = $("#filerNames").val().toUpperCase()

  if (filter == "@CHM") {
    window.location.href = "/booking?clubhausMode=true"
  }

  if (filter == "@ATM") {
    window.location.href = "/booking?anzeigeTafelMode=true"
  }

  var html = ""

  if (filter.length < 3) {
    html = "<li>Bitte gib mindestes 3 Buchstaben ein</li>"

  } else {
    for (var i in memberlist) {
      var member = memberlist[i]
      if (member.toUpperCase().indexOf(filter) >= 0 &&
        !memberIsPartOfBooking(member)) {
        var ssMember = member.split(";")
        var oneMemberHtml = `<li class='list-group-item'><button class='memberButton' value='${member}'>${ssMember[1]}</button></li>`;
        html += oneMemberHtml;
      }
    }
  }


  $("#names").html(html)

});


function showPartners(root, partners) {

  var html = "";
  for (var i in partners) {
    var partner = partners[i]
    var ss = partner.split(";")
    var oneMemberHtml = `<li>${ss[1]}</li>`;
    html += oneMemberHtml;
  }

  root.find(".partners").html(html)

}

function checkValidPartner(partner) {

  if (partner) {
    if (partner == "") {
      return false
    }
  } else {
    return false
  }


  if (clubhausMode) {
    //
    // im clubhausMode werden alle 2 / 4 Partner eingegeben
    //
    var id = parseInt(partner.split(";")[0])
    if (id < 20) {
      partner.superMember = true
      return "@special@"
    }
  } else {
    for (var i in partners) {
      if (partner == partners[i]) {
        return false
      }

    }
  }


  return true
}

function hidePartnerSelection() {
  $(".partnerSelection").hide()
}


function checkGastspieler() {
  try {
    var id = partners[partners.length - 1].split(";")[0]
    if (id == 9999) {
      gastModalCloseWithOk = false

      var user = JSON.parse(localStorage.getItem('user'))
      var memberId = localStorage.getItem('id');
      var pin = localStorage.getItem('pin');

      $.get(`booking/getIBAN?id=${memberId}`, function (data) {

        $("#GastIBAN").val(data.ibanF)
        $("#GastKontoInhaber").val(data.kontoInhaber)
        $("#GastBIC").val(data.bic)
        $("#GastBank").val(data.bank)
        $("#GastIstJugendlich")[0].checked = false

        var m = $("#gastModal");
        m.modal({keyboard: true})

        setTimeout(function () {
          $("body").css("cursor", "default");
          $("#gastName").focus()
        }, 500)

      })

    }
  } catch (ex) {}
}


function updateAfterMemberClickedAndChecked(checkResult) {
  var xHidePartnerSelection = false
  var xShowReservieren = false

  if (checkResult == "@special@") {
    xHidePartnerSelection = true
    xShowReservieren = true
  } else {
    if (clubhausMode) {
      //
      // im clubhausMode werden alle 2 / 4 Partner eingegeben
      //

      xHidePartnerSelection = (partners.length == 4)
      xShowReservieren = (partners.length == 2) || (partners.length == 4)
    } else {
      xHidePartnerSelection = (partners.length == 3)
      xShowReservieren = (partners.length == 1) || (partners.length == 3)
    }
  }



  if (xHidePartnerSelection) {
    hidePartnerSelection();
    $("#reservieren").focus()
  } else {
    $("#filerNames").val("")
    $("#names").html("")
    $("#filerNames").focus()
  }

  if (xShowReservieren) {
    $("#reservieren").attr("disabled", false)
  } else {
    $("#reservieren").attr("disabled", true)
  }

}

$(document).on("click", "#btnHideHotNews", function (ev) {
  var hashCode = $("#htmlHotNews").text().hashCode()
  localStorage.setItem('hotNewsHash', hashCode);

  $("#htmlHotNews").hide()
});

$(document).on("click", "#gotoFAQ", function (ev) {
  window.location.href = "booking/faq";
});


$(document).on("click", ".memberButton", function (ev) {

  var partner = ev.target.attributes.value.value

  var checkResult = checkValidPartner(partner)
  if (!checkResult) {
    return
  }

  var id = parseInt(partner.split(";")[0])
  if (id <= 4) {
    // special id for training ....
    // Serienbuchung ermöglichen

    $("#reservierenSerie").show()
  }


  bookingInfo.partners = partners.slice(0)
  bookingInfo.partners[bookingInfo.partners.length] = partner

  checkBookingAllowed(bookingInfo,
    function (errorMessage) {
      myAlert(errorMessage, "Reservierung nicht möglich")
      showPartners($("#bookingModal"), partners);
      checkGastspieler()
      updateAfterMemberClickedAndChecked(checkResult);
    },
    function () {
      partners[partners.length] = partner
      showPartners($("#bookingModal"), partners);
      checkGastspieler()
      updateAfterMemberClickedAndChecked(checkResult);
    })






});

$(window).resize(function () {
  var browserZoomLevel = Math.round(window.devicePixelRatio * 100);
  currentZoomLevel = browserZoomLevel / initalBrowserZoomLevel
});

////////////////////////////////////////////////////////

function myAlert(s, title) {

  var ss = s.split("\n")
  s = ss.join("<br/>")

  bootbox.confirm({
    title: title,
    message: s,
    buttons: {
      confirm: {
        label: 'Na gut ...',
        className: 'btn-success'
      }
    },
    callback: function (result) {
      if (result) {
      }
    }
  });
}

function myConfirm(s, okCallback) {

  bootbox.confirm({
    title: "Achtung",
    message: s,
    buttons: {
      confirm: {
        label: 'Ja',
        className: 'btn-success'
      }
    },
    callback: function (result) {
      if (result) {
        if (okCallback) {
          okCallback()
        }
      }
    }
  });
}



var _debugOutput = ""
function debugOutput(s) {
  _debugOutput += "<p class='debugOutput'>" + s + "</p>"
  $("#debugOutput").html(_debugOutput)
}
